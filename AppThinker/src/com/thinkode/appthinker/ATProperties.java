package com.thinkode.appthinker;

import javax.swing.*;
import java.io.*;
import java.util.Properties;

public class ATProperties extends Properties {

    private String _savePath;

    /**
     * Constructeur de ATProperties, propriétés de l'utilisateur
     */
    public ATProperties() {
        _savePath = "user.properties";
    }

    public ATProperties(String savePath) {
        _savePath = savePath;
    }

    /**
     * Charger les préférences utilisateur depuis un fichier de propriété
     */
    public void loadPreferencesFromFile() {
        InputStream input = null;
        try {
            input = new FileInputStream(_savePath);
            //Chargement des préférences du logiciel
            this.load(input);
        } catch (final IOException ex) {
            //Fichier de préférences utilisateur introuvable. Restauration des préférences par défaut.
            setToDefault();
            storeConfiguration();
            JOptionPane.showMessageDialog(null, "User preferences file '" + _savePath + "' not found. The default configuration has been restored.", _savePath + " not found", JOptionPane.ERROR_MESSAGE);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    JOptionPane.showMessageDialog(null, "Not found error", _savePath + " not found", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    /**
     * Remet à zéro les préférences utilisateur
     */
    public void setToDefault() {
        setProperty("version", "1.0.0");
        setProperty("showHomeAtStartup", "true");
        setProperty("isFirstLaunch", "true");
    }

    /**
     * Enregistre les préférences utilisateur dans un fichier
     */
    public void storeConfiguration() {
        OutputStream output = null;
        try {
            output = new FileOutputStream(_savePath);
            //Enregistrement des propriétés du logiciel
            this.store(output, null);
        } catch (final IOException io) {
            JOptionPane.showMessageDialog(null, "Unable to save the user preferences in '" + _savePath + "': " + io.getMessage());
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
