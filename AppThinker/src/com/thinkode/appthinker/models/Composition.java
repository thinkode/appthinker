package com.thinkode.appthinker.models;

import java.io.Serializable;

/**
 * Gère une composition d'un projet.
 *
 * @author V.BOULANGER
 */
public class Composition implements Serializable {
    public static int _compositionId = 0;

    protected int _id;
    protected String _name;
    protected boolean _needToSave = true;

    /**
     * Constructeur de la Composition.
     */
    public Composition() {
        _id = _compositionId++;
    }

    /**
     * Retourne le nom de la composition.
     *
     * @return Le nom de la composition.
     */
    public String getName() {
        return _name;
    }

    /**
     * Paramètre le nom de la composition.
     *
     * @param name Le nouveau nom.
     */
    public void setName(String name) {
        _name = name;
        needsToSave(true);
    }

    /**
     * Retourne l'identifiant de la composition.
     *
     * @return L'identifiant de la composition.
     */
    public int getId() {
        return _id;
    }

    /**
     * Retourne si la composition a besoin d'être sauvegardée
     *
     * @return Un booléen représentant l'affirmation
     */
    public boolean isNeededToSave() {
        return _needToSave;
    }

    /**
     * Inscrit si la composition a besoin d'être sauvegardée ou non
     *
     * @param save Le paramètre de sauvegarde
     */
    public void needsToSave(boolean save) {
        _needToSave = save;
    }
}
