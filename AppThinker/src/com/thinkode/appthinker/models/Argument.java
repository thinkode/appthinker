package com.thinkode.appthinker.models;

import java.io.Serializable;

/**
 * Gère un argument d'une méthode.
 *
 * @author V.BOULANGER
 */
public class Argument implements Serializable {

    public static int _argumentId;

    private int _id;
    private String _type;
    private String _name;

    /**
     * Constructeur - Crée une instance de Argument.
     */
    public Argument() {
        _argumentId++;
        this._id = _argumentId;
        this._type = "int";
        this._name = "argument" + this._id;
    }

    /**
     * Constructeur - Crée une instance de Argument avec des paramètres donnés.
     *
     * @param type Le type de l'argument.
     * @param name Le nom de l'argument.
     */
    public Argument(String type, String name) {
        _argumentId++;
        this._id = _argumentId;
        this._type = type;
        this._name = name;
    }

    /**
     * Récupère le numéro de l'argument.
     *
     * @return Le numéro de l'argument.
     */
    public int getId() {
        return _id;
    }

    /**
     * Récupère le type de l'argument.
     *
     * @return Le type de l'argument.
     */
    public String getType() {
        return _type;
    }

    /**
     * Paramètre le type de l'argument.
     *
     * @param type Le type de l'argument.
     */
    public void setType(String type) {
        this._type = type;
    }

    /**
     * Récupère le nom de l'argument.
     *
     * @return Le nom de l'argument.
     */
    public String getName() {
        return _name;
    }

    /**
     * Paramètre le nom de l'argument.
     *
     * @param name Le nom de l'argument.
     */
    public void setName(String name) {
        this._name = name;
    }
}
