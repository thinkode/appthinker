package com.thinkode.appthinker.models;

import java.io.Serializable;

/**
 * Gère un attribut d'une classe.
 *
 * @author V.BOULANGER
 */
public class Attribute implements Serializable {

    public static int _attributeId = 0;

    public static final String PRIVATE = "-";
    public static final String PROTECTED = "#";
    public static final String PUBLIC = "+";

    private int _id;
    private String _access;
    private String _type;
    private String _name;
    private boolean _isStatic;
    private boolean _isFinal;
    private boolean _isAbstract;
    private boolean _isSynchronized;
    private boolean _isVolatile;
    private boolean _isTransient;

    /**
     * Constructeur - Crée une instance de Attribute.
     */
    public Attribute() {
        _attributeId++;
        this._id = _attributeId;
        this._access = Attribute.PRIVATE;
        this._type = "int";
        this._name = "attribute" + this._id;
    }

    /**
     * Constructeur - Crée une instance de Attribute avec des paramètres donnés.
     *
     * @param access Le modificateur d'accès de l'attribut.
     * @param name   Le nom de l'attribut.
     * @param type   Le type de l'attribut.
     */
    public Attribute(String name, String access, String type) {
        _attributeId++;
        this._id = _attributeId;
        this._access = access;
        this._type = type;
        this._name = name;
    }

    /**
     * Récupère le numéro de l'attribut.
     *
     * @return Le numéro de l'attribut.
     */
    public int getId() {
        return _id;
    }

    /**
     * Récupère le modificateur d'accès de l'attribut.
     *
     * @return Le modificateur d'accès de l'attribut
     */
    public String getAccess() {
        return _access;
    }

    /**
     * Paramètre le modificateur d'accès de l'attribut.
     *
     * @param access Le modificateur d'accès de l'attribut
     */
    public void setAccess(String access) {
        this._access = access;
    }

    /**
     * Récupère le type de l'attribut.
     *
     * @return Le type de l'attribut.
     */
    public String getType() {
        return _type;
    }

    /**
     * Paramètre le type de l'attribut.
     *
     * @param type Le type de l'attribut.
     */
    public void setType(String type) {
        this._type = type;
    }

    /**
     * Récupère le nom de l'attribut.
     *
     * @return Le nom de l'attribut.
     */
    public String getName() {
        return _name;
    }

    /**
     * Paramètre le nom de l'attribut.
     *
     * @param name Le nom de l'attribut.
     */
    public void setName(String name) {
        this._name = name;
    }

    /**
     * Retourne si l'attribut est statique ou non.
     *
     * @return true : l'attribut est statique, false: l'attribut n'est pas statique.
     */
    public boolean isStatic() {
        return _isStatic;
    }

    /**
     * Paramètre la caractéristique statique de l'attribut.
     *
     * @param s true : l'attribut est statique, false : l'attribut n'est pas statique
     */
    public void setStatic(boolean s) {
        this._isStatic = s;
    }

    /**
     * Retourne si l'attribut est final ou non.
     *
     * @return true : l'attribut est final, false: l'attribut n'est pas final.
     */
    public boolean isFinal() {
        return _isFinal;
    }

    /**
     * Paramètre la caractéristique final de l'attribut.
     *
     * @param f true : l'attribut est final, false : l'attribut n'est pas final
     */
    public void setFinal(boolean f) {
        this._isFinal = f;
    }

    /**
     * Retourne si l'attribut est abstrait ou non.
     *
     * @return true : l'attribut est abstrait, false: l'attribut n'est pas abstrait.
     */
    public boolean isAbstract() {
        return _isAbstract;
    }

    /**
     * Paramètre la caractéristique abstraite de l'attribut.
     *
     * @param a true : l'attribut est abstrait, false : l'attribut n'est pas abstrait.
     */
    public void setAbstract(boolean a) {
        this._isAbstract = a;
    }

    /**
     * Retourne si l'attribut est synchronisé ou non.
     *
     * @return true : l'attribut est synchronisé, false: l'attribut n'est pas synchronisé.
     */
    public boolean isSynchronized() {
        return _isSynchronized;
    }

    /**
     * Paramètre la caractéristique synchronisée de l'attribut.
     *
     * @param s true : l'attribut est synchronisé, false : l'attribut n'est pas synchronisé.
     */
    public void setSynchronized(boolean s) {
        this._isSynchronized = s;
    }

    /**
     * Retourne si l'attribut est volatile ou non.
     *
     * @return true : l'attribut est volatile, false: l'attribut n'est pas volatile.
     */
    public boolean isVolatile() {
        return _isVolatile;
    }

    /**
     * Paramètre la caractéristique volatile de l'attribut.
     *
     * @param v true : l'attribut est volatile, false : l'attribut n'est pas volatile
     */
    public void setVolatile(boolean v) {
        this._isVolatile = v;
    }

    /**
     * Retourne si l'attribut est transitoire ou non.
     *
     * @return true : l'attribut est transitoire, false: l'attribut n'est pas transitoire.
     */
    public boolean isTransient() {
        return _isTransient;
    }

    /**
     * Paramètre la caractéristique transitoire de l'attribut.
     *
     * @param t true : l'attribut est transitoire, false : l'attribut n'est pas transitoire
     */
    public void setTransient(boolean t) {
        this._isTransient = t;
    }
}
