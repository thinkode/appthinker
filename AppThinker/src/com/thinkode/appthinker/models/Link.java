package com.thinkode.appthinker.models;

import com.thinkode.appthinker.views.UmlDiagramFrame;

import java.io.Serializable;

/**
 * Gère un lien entre deux classes.
 *
 * @author V.BOULANGER
 */
public class Link implements Serializable {

    public static int _linkId = 0;

    public static final int CARD_UNLIMITED = -1;
    public static final int CARD_NULL = 0;
    public static final int CARD_ONE = 1;

    public enum LinkType {
        STRONG,
        WEAK,
        COMPOSITION,
        AGGREGATION,
        INHERITANCE
    }

    private int _id;
    private LinkType _type;
    private String _name;

    private Class _start;
    private int _minCardinalityStart;
    private int _maxCardinalityStart;
    private UmlDiagramFrame.ClassGrip _gripStart;

    private Class _end;
    private int _minCardinalityEnd;
    private int _maxCardinalityEnd;
    private UmlDiagramFrame.ClassGrip _gripEnd;

    /**
     * Constructeur - Crée une instance d'un lien.
     *
     * @param start La classe de départ.
     * @param end   La classe d'arrivée.
     */
    public Link(Class start, Class end) {
        _linkId++;
        this._id = _linkId;
        this._start = start;
        this._end = end;
        this._name = "relation" + _linkId;
        this._minCardinalityStart = Link.CARD_NULL;
        this._maxCardinalityStart = Link.CARD_UNLIMITED;
        this._minCardinalityEnd = Link.CARD_NULL;
        this._maxCardinalityEnd = Link.CARD_UNLIMITED;
        this._type = LinkType.STRONG;
    }

    /**
     * Constructeur - Crée une instance d'un lien avec des paramètres donnés.
     *
     * @param start               La classe de départ.
     * @param end                 La classe d'arrivée.
     * @param gripStart           Le point d'accroche sur la classe de départ.
     * @param gripEnd             Le point d'accroche sur la classe d'arrivée.
     * @param minCardinalityStart La cardinalité minimum de la classe de départ.
     * @param maxCardinalityStart La cardinalité maximuù de la classe de départ.
     * @param minCardinalityEnd   La cardinalité minimum de la classe d'arrivée.
     * @param maxCardinalityEnd   La cardinalité maximum de la classe d'arrivée.
     * @param type                Le type de lien.
     */
    public Link(Class start, Class end, UmlDiagramFrame.ClassGrip gripStart, UmlDiagramFrame.ClassGrip gripEnd, int minCardinalityStart, int maxCardinalityStart, int minCardinalityEnd, int maxCardinalityEnd, LinkType type) {
        _linkId++;
        this._id = _linkId;
        this._start = start;
        this._end = end;
        this._gripStart = gripStart;
        this._gripEnd = gripEnd;
        this._minCardinalityStart = minCardinalityStart;
        this._maxCardinalityStart = maxCardinalityStart;
        this._minCardinalityEnd = minCardinalityEnd;
        this._maxCardinalityEnd = maxCardinalityEnd;
        this._type = type;
        this._name = "relation" + _linkId;
    }

    /**
     * Récupère le numéro du lien.
     *
     * @return Le numéro du lien.
     */
    public int getId() {
        return _id;
    }

    /**
     * Récupère la classe de départ du lien.
     *
     * @return La classe de départ du lien.
     */
    public Class getStart() {
        return _start;
    }

    /**
     * Paramètre la classe de départ du lien.
     *
     * @param start La classe de départ du lien.
     */
    public void setStart(Class start) {
        this._start = start;
    }

    /**
     * Récupère la cardinalité minimum de la classe de départ.
     *
     * @return La cardinalité minimum de la classe de départ.
     */
    public int getMinCardinalityStart() {
        return _minCardinalityStart;
    }

    /**
     * Paramètre la cardinalité minimum de la classe de départ.
     *
     * @param minCardinalityStart La cardinalité minimum de la classe de départ.
     */
    public void setMinCardinalityStart(int minCardinalityStart) {
        this._minCardinalityStart = minCardinalityStart;
    }

    /**
     * Récupère la cardinalité maximum de la classe de départ.
     *
     * @return La cardinalité maximum de la classe de départ.
     */
    public int getMaxCardinalityStart() {
        return _maxCardinalityStart;
    }

    /**
     * Paramètre la cardinalité maximum de la classe de départ.
     *
     * @param maxCardinalityStart La cardinalité maximum de la classe de départ.
     */
    public void setMaxCardinalityStart(int maxCardinalityStart) {
        this._maxCardinalityStart = maxCardinalityStart;
    }

    /**
     * Retourne le point d'accroche du lien sur la classe de départ.
     *
     * @return Le point d'accroche du lien sur la classe de départ.
     */
    public UmlDiagramFrame.ClassGrip getGripStart() {
        return _gripStart;
    }

    /**
     * Paramètre le point d'accroche du lien sur la classe de départ.
     *
     * @param gripStart Le point d'accroche du lien sur la classe de départ.
     */
    public void setGripStart(UmlDiagramFrame.ClassGrip gripStart) {
        _gripStart = gripStart;
    }

    /**
     * Récupère la classe d'arrivée du lien.
     *
     * @return La classe d'arrivée du lien.
     */
    public Class getEnd() {
        return _end;
    }

    /**
     * Paramètre la classe d'arrivée.
     *
     * @param end La classe d'arrivée.
     */
    public void setEnd(Class end) {
        this._end = end;
    }

    /**
     * Récupère la cardinalité minimum de la classe d'arrivée.
     *
     * @return La cardinalité minimum de la classe d'arrivée.
     */
    public int getMinCardinalityEnd() {
        return _minCardinalityEnd;
    }

    /**
     * Paramètre la cardinalité minimum de la classe d'arrivée.
     *
     * @param minCardinalityEnd La cardinalité minimum de la classe d'arrivée.
     */
    public void setMinCardinalityEnd(int minCardinalityEnd) {
        this._minCardinalityEnd = minCardinalityEnd;
    }

    /**
     * Récupère la cardinalité maximum de la classe d'arrivée.
     *
     * @return La cardinalité maximum de la classe d'arrivée.
     */
    public int getMaxCardinalityEnd() {
        return _maxCardinalityEnd;
    }

    /**
     * Paramètre la cardinalité maximum de la classe d'arrivée.
     *
     * @param maxCardinalityEnd La cardinalité maximum de la classe d'arrivée.
     */
    public void setMaxCardinalityEnd(int maxCardinalityEnd) {
        this._maxCardinalityEnd = maxCardinalityEnd;
    }

    /**
     * Retourne le point d'accroche du lien sur la classe d'arrivée.
     *
     * @return Le point d'accroche du lien sur la classe d'arrivée.
     */
    public UmlDiagramFrame.ClassGrip getGripEnd() {
        return _gripEnd;
    }

    /**
     * Paramètre le point d'accroche du lien sur la classe d'arrivée.
     *
     * @param gripEnd Le point d'accroche du lien sur la classe d'arrivée.
     */
    public void setGripEnd(UmlDiagramFrame.ClassGrip gripEnd) {
        _gripEnd = gripEnd;
    }

    /**
     * Récupère le type du lien.
     *
     * @return Le type du lien.
     */
    public LinkType getType() {
        return _type;
    }

    /**
     * Paramètre le type du lien
     *
     * @param type Le type du lien.
     */
    public void setType(LinkType type) {
        this._type = type;
    }

    /**
     * Récupère le nom du lien.
     *
     * @return Le nom du lien.
     */
    public String getName() {
        return _name;
    }

    /**
     * Paramètre le nom du lien
     *
     * @param name Le nom du lien.
     */
    public void setName(String name) {
        this._name = name;
    }

    /**
     * Modifie la direction du lien (permute les classes de départ et d'arrivée).
     */
    public void switchDirection() {
        Class start = this._start;
        int minStart = this._minCardinalityStart;
        int maxStart = this._maxCardinalityStart;
        this._start = _end;
        this._end = start;
        this._minCardinalityStart = _minCardinalityEnd;
        this._maxCardinalityStart = _maxCardinalityEnd;
        this._minCardinalityEnd = minStart;
        this._maxCardinalityEnd = maxStart;
        UmlDiagramFrame.ClassGrip gripStart = this._gripStart;
        this._gripStart = this._gripEnd;
        this._gripEnd = gripStart;
    }
}
