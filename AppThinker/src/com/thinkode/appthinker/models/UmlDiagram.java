package com.thinkode.appthinker.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Gère un diagramme UML.
 *
 * @author V.BOULANGER
 */
public class UmlDiagram extends Composition {

    private List<Class> _classes;
    private List<Link> _links;
    private Class _mainClass = null;

    /**
     * Constructeur de UmlDiagram
     */
    public UmlDiagram() {
        _name = "UML Diagram " + _compositionId;
        _classes = new ArrayList<Class>();
        _links = new ArrayList<Link>();
    }

    /**
     * Retourne une liste de toutes les classes du diagramme UML.
     *
     * @return Une liste de toutes les classes du diagramme UML.
     */
    public List<Class> getClasses() {
        return _classes;
    }

    /**
     * Ajoute une classe au diagramme UML.
     *
     * @param a La classe à ajouter.
     */
    public void addClass(Class a) {
        _classes.add(a);
        //On vient d'ajouter la 1ère classe, définition en classe principale
        if (_classes.size() == 1) _mainClass = a;
    }

    /**
     * Supprime une classe du diagramme UML.
     *
     * @param a La classe à supprimer.
     */
    public void removeClass(Class a) {
        //Suppression des liens menant vers la classe à supprimer
        Iterator<Link> iter = getLinks().iterator();
        while (iter.hasNext()) {
            Link link = iter.next();
            if (a.equals(link.getStart()) || a.equals(link.getEnd())) iter.remove();
        }
        //Suppression de la classe
        _classes.remove(a);
        //Si la classe supprimée était la classe principale, on définit la 2ème classe en tant que classe principale.
        if (_mainClass == a && _classes.size() != 0) _mainClass = _classes.get(0);
    }

    /**
     * Supprime toutes les classes du diagramme UML.
     */
    public void clearClasses() {
        _classes.clear();
        clearLinks();
    }

    /**
     * Retourne une liste de tous les liens du diagramme UML.
     *
     * @return Une liste de tous les liens du diagramme UML.
     */
    public List<Link> getLinks() {
        return _links;
    }

    /**
     * Ajoute un lien au diagramme UML.
     *
     * @param a Le lien à ajouter.
     */
    public void addLink(Link a) {
        _links.add(a);
    }

    /**
     * Supprime un lien du diagramme UML.
     *
     * @param a Le lien à supprimer.
     */
    public void removeLink(Link a) {
        _links.remove(a);
    }

    /**
     * Supprimer tous les liens du diagramme UML.
     */
    public void clearLinks() {
        _links.clear();
    }

    /**
     * Retourne la classe principale du diagramme.
     *
     * @return La classe principale du diagramme.
     */
    public Class getMainClass() {
        return _mainClass;
    }

    /**
     * Paramétre la classe principale du diagramme.
     *
     * @param a La nouvelle classe principale du diagramme.
     */
    public void setMainClass(Class a) {
        _mainClass = a;
    }
}
