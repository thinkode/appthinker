package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.views.HomeFrame;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class HomeFrameController {

    WindowController _windowController;
    HomeFrame _homeFrame;

    /**
     * Constructeur de HomeFrameController.
     *
     * @param homeFrame        Une instance de la page d'accueil.
     * @param windowController Le contrôleur de la fenêtre principale qui a demandé l'affichage
     */
    public HomeFrameController(HomeFrame homeFrame, WindowController windowController) {
        _windowController = windowController;
        _homeFrame = homeFrame;
        _homeFrame.setController(this);
    }

    /**
     * Ajoute un projet vide à la fenêtre.
     */
    public void newBlankProject() {
        _windowController.newBlankProject();
    }

    /**
     * Ajoute un projet UML à la fenêtre.
     */
    public void newUmlProject() {
        _windowController.newUmlProject();
    }

    /**
     * Commande l'ouverture d'un projet existant.
     *
     * @param path Le chemin du projet.
     */
    public void openProject(String path) {
        _windowController.openProject(path);
    }

    /**
     * Lance l'URL du formumlaire de soumission d'idées ou bugs.
     */
    public void giveFeedback() {
        try {
            Desktop.getDesktop().browse(new URL("https://forms.gle/WG32HT947MKqdzhbA").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to give a feedback : " + ex.toString());
        }
    }

    /**
     * Lance l'URL du dépôt.
     */
    public void visitRepository() {
        try {
            Desktop.getDesktop().browse(new URL("https://gitlab.com/thinkode/appthinker").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to visit the repository : " + ex.toString());
        }
    }

    /**
     * Lance l'URL pour faire un don.
     */
    public void makeDonation() {
        try {
            Desktop.getDesktop().browse(new URL("https://www.paypal.com/paypalme/valentinboulanger").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to make a donation : " + ex.toString());
        }
    }

    //Actions sur la checkbox

    /**
     * Modifie le paramètre utilisateur pour l'ouverture automatique de la page d'accueil au démarrage.
     *
     * @param state L'état.
     */
    public void showHomeAtStartup(boolean state) {
        String stringState = (!state) ? "true" : "false";
        AppThinker.getATProperties().setProperty("showHomeAtStartup", stringState);
        AppThinker.getATProperties().storeConfiguration();
    }

    /**
     * Retourne le paramètre utilisateur d'ouverture automatique de la page d'accueil.
     *
     * @return L'état.
     */
    public boolean isHomeAtStartup() {
        String state = AppThinker.getATProperties().getProperty("showHomeAtStartup");
        return (state.equals("true")) ? true : false;
    }
}
