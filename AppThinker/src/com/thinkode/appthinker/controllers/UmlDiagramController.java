package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.models.Class;
import com.thinkode.appthinker.models.Link;
import com.thinkode.appthinker.models.UmlDiagram;
import com.thinkode.appthinker.views.UmlDiagramFrame;

import java.util.List;

public class UmlDiagramController {

    private UmlDiagramFrame _umlDiagramFrame;
    private UmlDiagram _umlDiagram;

    /**
     * Constructeur de UmlDiagramController
     *
     * @param umlDiagramFrame La fenêtre contrôlée.
     * @param umlDiagram      Le diagramme UML concerné.
     */
    public UmlDiagramController(UmlDiagramFrame umlDiagramFrame, UmlDiagram umlDiagram) {
        _umlDiagramFrame = umlDiagramFrame;
        _umlDiagram = umlDiagram;
        _umlDiagramFrame.setController(this);
    }

    /**
     * Retourne le nom du diagramme UML.
     *
     * @return Le nom du diagramme UML.
     */
    public String getName() {
        return _umlDiagram.getName();
    }

    /**
     * Retourne la liste des classes du diagramme UML.
     *
     * @return La liste des classes du diagramme UML.
     */
    public List<Class> getClassesList() {
        return _umlDiagram.getClasses();
    }

    /**
     * Retire une classe du diagramme.
     *
     * @param a La classe à retirer.
     */
    public void removeClass(Class a) {
        _umlDiagram.removeClass(a);
        needToSave();
    }

    /**
     * Retire un lien du diagramme.
     *
     * @param l Le lien à retirer.
     */
    public void removeLink(Link l) {
        _umlDiagram.removeLink(l);
        needToSave();
    }

    /**
     * Supprime l'ensemble des classes du diagramme.
     */
    public void clearClasses() {
        _umlDiagram.clearClasses();
        needToSave();
    }

    /**
     * Retourne la liste des liens du diagramme.
     *
     * @return La liste des liens du diagramme.
     */
    public List<Link> getLinksList() {
        return _umlDiagram.getLinks();
    }

    /**
     * Retourne la classe principale du diagramme.
     *
     * @return La classe principale du diagramme.
     */
    public Class getMainClass() {
        return _umlDiagram.getMainClass();
    }

    /**
     * Paramètre la classe principale du diagramme.
     *
     * @param c La classe principale.
     */
    public void setMainClass(Class c) {
        _umlDiagram.setMainClass(c);
    }

    /**
     * Ajoute une classe dans le diagramme
     *
     * @param posX La coordonnée X de la classe
     * @param posY La coordonnée Y de la classe
     */
    public void addClass(int posX, int posY) {
        _umlDiagram.addClass(new Class(posX, posY));
        needToSave();
    }

    /**
     * Redimensionne une classe vers le haut.
     *
     * @param a    La classe à redimensionner.
     * @param posY Le décalage Y de la souris.
     */
    public void resizeUp(Class a, int posY) {
        a.resizeUp(posY);
        needToSave();
    }

    /**
     * Redimensionne une classe vers la droite.
     *
     * @param a    La classe à redimensionner.
     * @param posX Le décalage X de la souris.
     */
    public void resizeRight(Class a, int posX) {
        a.resizeRight(posX);
        needToSave();
    }

    /**
     * Redimensionne une classe vers le bas.
     *
     * @param a    La classe à redimensionner.
     * @param posY Le décalage Y de la souris.
     */
    public void resizeDown(Class a, int posY) {
        a.resizeDown(posY);
        needToSave();
    }

    /**
     * Redimensionne une classe vers la gauche.
     *
     * @param a    La classe à redimensionner.
     * @param posX Le décalage X de la souris.
     */
    public void resizeLeft(Class a, int posX) {
        a.resizeLeft(posX);
        needToSave();
    }

    /**
     * Paramètre la position X d'une classe du diagramme.
     *
     * @param a    La classe à modifier.
     * @param posX La nouvelle position X.
     */
    public void setPosX(Class a, int posX) {
        a.setPosX(posX);
        needToSave();
    }

    /**
     * Paramètre la position Y d'une classe du diagramme.
     *
     * @param a    La classe à modifier.
     * @param posY La nouvelle position Y.
     */
    public void setPosY(Class a, int posY) {
        a.setPosY(posY);
        needToSave();
    }

    /**
     * Ajoute un nouveau lien au diagramme.
     *
     * @param start        La classe de départ
     * @param end          La classe d'arrivée
     * @param gripStart    La position de départ
     * @param gripEnd      La position d'arrivée
     * @param minCardStart La cardinalité minimum de la classe de départ
     * @param maxCardStart La cardinalité maximum de la classe de départ
     * @param minCardEnd   La cardinalité minimum de la classe d'arrivée
     * @param maxCardEnd   La cardinalité maximum de la classe d'arrivée
     * @param type         Le type du lien
     */
    public void addLink(Class start, Class end, UmlDiagramFrame.ClassGrip gripStart, UmlDiagramFrame.ClassGrip gripEnd, int minCardStart, int maxCardStart, int minCardEnd, int maxCardEnd, Link.LinkType type) {
        _umlDiagram.addLink(new Link(start, end, gripStart, gripEnd, minCardStart, maxCardStart, minCardEnd, maxCardEnd, type));
        needToSave();
    }

    /**
     * Invalidation du diagramme pour sauvegarde
     */
    public void needToSave() {
        _umlDiagram.needsToSave(true);
        _umlDiagramFrame.needWorkspaceRefresh();
    }

    /**
     * Rafraîchissement du diagramme
     */
    public void refreshGraphics() {
        _umlDiagramFrame.redraw();
    }

    /**
     * Affichage de la fenêtre de modification d'une classe
     *
     * @param c La classe à modifier
     */
    public void showClassWindow(Class c) {
        new ClassPropertiesController(this, c);
    }

    /**
     * Affichage de la fenêtre de modification d'un lien
     *
     * @param l Le lien à modifier
     */
    public void showLinkWindow(Link l) {
        new LinkPropertiesController(this, l);
    }
}