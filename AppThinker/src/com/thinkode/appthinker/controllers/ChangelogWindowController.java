package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.views.ChangelogWindow;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class ChangelogWindowController {

    private ChangelogWindow _atChangelogWindow;

    /**
     * Constructeur de ChangelogWindowController.
     */
    public ChangelogWindowController() {
        _atChangelogWindow = new ChangelogWindow();
        _atChangelogWindow.setController(this);
        _atChangelogWindow.displayChangelog();
    }

    /**
     * Renvoie le numéro de version du logiciel à la fenêtre.
     *
     * @return Le numéro de version du logiciel.
     */
    public String getVersion() {
        return AppThinker.version;
    }

    /**
     * Renvoie les dernières modifications apportées au logiciel à la fenêtre.
     *
     * @return Les dernières modifications apportées au logiciel.
     */
    public java.util.List<String> getChangelog() {
        return AppThinker.changelog;
    }

    /**
     * Lance l'URL du formulaire pour la soumission d'idées ou bugs.
     */
    public void giveFeedback() {
        try {
            Desktop.getDesktop().browse(new URL("https://forms.gle/WG32HT947MKqdzhbA").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to give a feedback : " + ex.toString());
        }
    }
}
