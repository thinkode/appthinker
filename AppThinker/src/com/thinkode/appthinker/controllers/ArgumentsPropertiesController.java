package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.models.Argument;
import com.thinkode.appthinker.models.Method;
import com.thinkode.appthinker.views.ArgumentsPropertiesWindow;

public class ArgumentsPropertiesController {

    private ArgumentsPropertiesWindow _argumentsPropertiesWindow;
    private UmlDiagramController _umlDiagramController;
    private ClassPropertiesController _classPropertiesController;
    private Method _method;

    /**
     * Constructeur de ArgumentsPropertiesController
     *
     * @param umlDiagramController      Une instance de UmlDiagramController qui a effectuée la demande
     * @param classPropertiesController Une instance de ClassPropertiesController
     * @param method                    La méthode modifiée
     */
    public ArgumentsPropertiesController(UmlDiagramController umlDiagramController, ClassPropertiesController classPropertiesController, Method method) {
        _umlDiagramController = umlDiagramController;
        _classPropertiesController = classPropertiesController;
        _method = method;
        _argumentsPropertiesWindow = new ArgumentsPropertiesWindow();
        _argumentsPropertiesWindow.setController(this);
        _argumentsPropertiesWindow.initializeGraphics();
    }

    /**
     * Renvoie à la fenêtre le nom de la méthode en cours d'édition
     *
     * @return Le nom de la méthode en cours d'édition.
     */
    public String getMethodName() {
        return _method.getName();
    }

    /**
     * Ajoute un argument à la méthode sélectionnée et rafraîchit la liste des arguments.
     */
    public void addArgument() {
        _argumentsPropertiesWindow.save();
        _method.addArgument(new Argument());
        _classPropertiesController.computeMinSize();
        _umlDiagramController.refreshGraphics();
        refreshArguments();
        _umlDiagramController.needToSave();
    }

    /**
     * Retire un argument à la méthode et rafraîchit la liste des arguments.
     *
     * @param index Le numéro de l'argument.
     */
    public void removeArgument(int index) {
        if (index != -1) {
            _argumentsPropertiesWindow.save();
            _method.removeArgument(index);
            _classPropertiesController.computeMinSize();
            _umlDiagramController.refreshGraphics();
            refreshArguments();
            _umlDiagramController.needToSave();
        }
    }

    /**
     * Monte l'argument
     *
     * @param index L'index de la méthode dans le tableau.
     */
    public void goUpArgument(int index) {
        if (_method.upArgument(index)) {
            _umlDiagramController.refreshGraphics();
            refreshArguments();
            _argumentsPropertiesWindow.selectArgument(index - 1);
            _umlDiagramController.needToSave();
        }
    }

    /**
     * Baisse l'argument
     *
     * @param index L'index de la méthode dans le tableau.
     */
    public void goDownArgument(int index) {
        if (_method.downArgument(index)) {
            _umlDiagramController.refreshGraphics();
            refreshArguments();
            _argumentsPropertiesWindow.selectArgument(index + 1);
            _umlDiagramController.needToSave();
        }
    }

    /**
     * Commande le rafraîchissement du tableau des arguments
     */
    public void refreshArguments() {
        _argumentsPropertiesWindow.listArguments(_method.getArguments());
    }

    /**
     * Sauvegarde les modifications pour la méthode en cours et ferme la fenêtre.
     *
     * @param index Le numéro de l'argument à sauvegarder.
     * @param type  Le type de l'argument à sauvegarder.
     * @param name  Le nom de l'argument à sauvegarder.
     */
    public void save(int index, String type, String name) {
        _method.getArguments().get(index).setType(type);
        _method.getArguments().get(index).setName(name);
        //Rafraichissement de l'affichage
        _classPropertiesController.computeMinSize();
        _umlDiagramController.refreshGraphics();
        _umlDiagramController.needToSave();
    }
}
