package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.views.AboutWindow;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class AboutWindowController {

    private AboutWindow _atAboutWindow;

    /**
     * Constructeur de AboutWindowController
     */
    public AboutWindowController() {
        _atAboutWindow = new AboutWindow();
        _atAboutWindow.setController(this);
        _atAboutWindow.initializeGraphics();
    }

    /**
     * Lance l'URL du centre de téléchargement.
     */
    public void checkForUpdates() {
        try {
            Desktop.getDesktop().browse(new URL("https://insset-my.sharepoint.com/:f:/g/personal/valentin_boulanger_insset_onmicrosoft_com/En4LybbeF2ZMpeSiesh_Af8BX3Fl1aDzTUWyw4dtQzJaag").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to check for updates : " + ex.toString());
        }
    }

    /**
     * Lance la fenetre de Changelog
     */
    public void launchChangelogWindow() {
        new ChangelogWindowController();
    }

    /**
     * Lance l'URL du formulaire pour la soumission d'idées ou bugs
     */
    public void giveFeedback() {
        try {
            Desktop.getDesktop().browse(new URL("https://forms.gle/WG32HT947MKqdzhbA").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to give a feedback : " + ex.toString());
        }
    }

    /**
     * Lance l'URL pour faire un don.
     */
    public void makeDonation() {
        try {
            Desktop.getDesktop().browse(new URL("https://www.paypal.com/paypalme/valentinboulanger").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to make a donation : " + ex.toString());
        }
    }
}
