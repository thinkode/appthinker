package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.models.Argument;
import com.thinkode.appthinker.models.Attribute;
import com.thinkode.appthinker.models.Method;
import com.thinkode.appthinker.views.ClassPropertiesWindow;

import java.util.ArrayList;

public class ClassPropertiesController {

    private ClassPropertiesWindow _classPropertiesWindow;
    private UmlDiagramController _umlDiagramController;
    private com.thinkode.appthinker.models.Class _class;

    /**
     * Constructeur de ClassPropertiesController
     *
     * @param umlDiagramController Le contrôleur du diagramme UML qui a demandée l'affichage.
     * @param c                    La classe en cours d'édition.
     */
    public ClassPropertiesController(UmlDiagramController umlDiagramController, com.thinkode.appthinker.models.Class c) {
        _class = c;
        _umlDiagramController = umlDiagramController;
        _classPropertiesWindow = new ClassPropertiesWindow();
        _classPropertiesWindow.setController(this);
        _classPropertiesWindow.initializeGraphics();
    }

    /**
     * Donne le nom de la classe en cours d'édition à la fenêtre.
     *
     * @return Le nom de la classe en cours d'édition.
     */
    public String getClassName() {
        return _class.getName();
    }

    /**
     * Paramètre la classe en cours d'édition comme classe principale
     */
    public void setMainClass() {
        _umlDiagramController.setMainClass(_class);
        _umlDiagramController.needToSave();
        refreshGraphics();
    }

    /**
     * Retourne si la classe en cours d'édition est la classe principale
     *
     * @return Un booléen représentant l'affirmation.
     */
    public boolean isMainClass() {
        return _umlDiagramController.getMainClass() == _class;
    }

    /**
     * Recalcule les dimensions minimum de la classe
     */
    public void computeMinSize() {
        _class.computeMinSize();
    }

    /**
     * Ajoute un attribut à la classe et rafraîchit la liste des attributs.
     */
    public void addAttribute() {
        _classPropertiesWindow.saveAttributes();
        _class.addAttribute(new Attribute());
        refreshAttributes();
        _umlDiagramController.needToSave();
        refreshGraphics();
    }

    /**
     * Retire un attribut à la classe et rafraîchit la liste des attributs.
     *
     * @param index Le numéro de l'attribut dans le tableau.
     */
    public void removeAttribute(int index) {
        if (index != -1) {
            _classPropertiesWindow.saveAttributes();
            _class.removeAttribute(index);
            refreshAttributes();
            _umlDiagramController.needToSave();
            refreshGraphics();
        }
    }

    /**
     * Sauvegarde les informations d'un attribut.
     *
     * @param index          Le numéro de l'attribut à modifier.
     * @param name           Le nom de l'attribut.
     * @param access         L'accès de l'attribut.
     * @param type           Le type de l'attribut.
     * @param isStatic       L'attribut est statique.
     * @param isFinal        L'attribut est final.
     * @param isAbstract     L'attribut est abstrait.
     * @param isSynchronized L'attribut est synchronisé.
     * @param isVolatile     L'attribut est volatile.
     * @param isTransient    L'attribut est éphémère.
     */
    public void saveAttribute(int index, String name, String access, String type, boolean isStatic, boolean isFinal, boolean isAbstract, boolean isSynchronized, boolean isVolatile, boolean isTransient) {
        _class.getAttributes().get(index).setName(name);
        _class.getAttributes().get(index).setAccess(access);
        _class.getAttributes().get(index).setType(type);
        _class.getAttributes().get(index).setStatic(isStatic);
        _class.getAttributes().get(index).setFinal(isFinal);
        _class.getAttributes().get(index).setAbstract(isAbstract);
        _class.getAttributes().get(index).setSynchronized(isSynchronized);
        _class.getAttributes().get(index).setVolatile(isVolatile);
        _class.getAttributes().get(index).setTransient(isTransient);
    }

    /**
     * Go up the selected attribute
     *
     * @param attributeRow Le numéro de l'attribut.
     */
    public void goUpAttribute(int attributeRow) {
        if (_class.upAttribute(attributeRow)) {
            refreshGraphics();
            refreshAttributes();
            _classPropertiesWindow.selectAttribute(attributeRow - 1);
            _umlDiagramController.needToSave();
        }
    }

    /**
     * Go down the selected attribute
     *
     * @param attributeRow Le numéro de l'attribut.
     */
    public void goDownAttribute(int attributeRow) {
        if (_class.downAttribute(attributeRow)) {
            refreshGraphics();
            refreshAttributes();
            _classPropertiesWindow.selectAttribute(attributeRow + 1);
            _umlDiagramController.needToSave();
        }
    }

    /**
     * Open the arguments properties window
     *
     * @param methodRow Le numéro de la méthode.
     */
    public void openArgumentsWindow(int methodRow) {
        new ArgumentsPropertiesController(_umlDiagramController, this, _class.getMethods().get(methodRow));
    }


    /**
     * Rafraîchissement des attributs
     */
    public void refreshAttributes() {
        _classPropertiesWindow.listAttributes(_class.getAttributes());
    }

    /**
     * Rafraîchissement des méthodes
     */
    public void refreshMethods() {
        _classPropertiesWindow.listMethods(_class.getMethods());
    }

    /**
     * Ajoute une méthode à la classe et rafraîchit la liste des méthodes.
     */
    public void addMethod() {
        _classPropertiesWindow.saveMethods();
        _class.addMethod(new Method());
        refreshMethods();
        _umlDiagramController.needToSave();
        refreshGraphics();
    }

    /**
     * Ajoute une méthode à la classe à une position donnée et rafraîchit la liste des attributs.
     *
     * @param index Le numéro de la méthode.
     * @param m     La méthode à insérer.
     */
    private void addMethod(int index, Method m) {
        _classPropertiesWindow.saveMethods();
        _class.addMethod(index, m);
        _umlDiagramController.needToSave();
        refreshMethods();
        refreshGraphics();
    }

    /**
     * Retire une méthode à la classe et rafraîchit la liste des méthodes.
     *
     * @param index Le numéro de la méthode.
     */
    public void removeMethod(int index) {
        if (index != -1) {
            _classPropertiesWindow.saveMethods();
            _class.removeMethod(index);
            _umlDiagramController.needToSave();
            refreshMethods();
            refreshGraphics();
        }
    }

    /**
     * Sauvegarde les informations d'une méthode
     *
     * @param index          L'index de la méthode
     * @param isConstructor  La méthode est constructeur
     * @param name           Le nom de la méthode
     * @param access         L'accès de la méthode
     * @param type           Le type de la méthode
     * @param isStatic       La méthode est statique
     * @param isFinal        La méthode est finale
     * @param isAbstract     La méthode est abstraite
     * @param isSynchronized La méthode est synchronisée
     * @param isVolatile     La méthode est volatile
     * @param isTransient    La méthode est éphémère
     */
    public void saveMethod(int index, boolean isConstructor, String name, String access, String type, boolean isStatic, boolean isFinal, boolean isAbstract, boolean isSynchronized, boolean isVolatile, boolean isTransient) {
        _class.getMethods().get(index).setConstructor(isConstructor);
        _class.getMethods().get(index).setName(name);
        _class.getMethods().get(index).setAccess(access);
        _class.getMethods().get(index).setType(type);
        _class.getMethods().get(index).setStatic(isStatic);
        _class.getMethods().get(index).setFinal(isFinal);
        _class.getMethods().get(index).setAbstract(isAbstract);
        _class.getMethods().get(index).setSynchronized(isSynchronized);
        _class.getMethods().get(index).setVolatile(isVolatile);
        _class.getMethods().get(index).setTransient(isTransient);
    }

    /**
     * Monter une méthode
     *
     * @param methodRow L'index de la méthode
     */
    public void goUpMethod(int methodRow) {
        if (_class.upMethod(methodRow)) {
            refreshGraphics();
            refreshMethods();
            _classPropertiesWindow.selectMethod(methodRow - 1);
            _umlDiagramController.needToSave();
        }
    }

    /**
     * Baisser une méthode
     *
     * @param methodRow L'index de la méthode
     */
    public void goDownMethod(int methodRow) {
        if (_class.downMethod(methodRow)) {
            refreshGraphics();
            refreshMethods();
            _classPropertiesWindow.selectMethod(methodRow + 1);
            _umlDiagramController.needToSave();
        }
    }

    /**
     * Surcharge la méthode sélectionnée
     *
     * @param index L'index de la méthode.
     */
    public void overloadMethod(int index) {
        if (index != -1) {
            Method m = _class.getMethods().get(index);
            java.util.List<Argument> args = new ArrayList<Argument>();
            //On ajoute un argument de plus que la méthode à surcharger
            args.addAll(m.getArguments());
            args.add(new Argument());
            Method m1 = new Method(m.getAccess(), m.getType(), m.getName(), args, m.isConstructor(), m.isStatic(), m.isFinal(), m.isAbstract(), m.isSynchronized(), m.isVolatile(), m.isTransient());
            addMethod(index + 1, m1);
            _umlDiagramController.needToSave();
            //this.openArgumentsWindow(m1);
        }
    }

    /**
     * Sauvegarde l'ensemble de la classe.
     *
     * @param name Le nom de la classe.
     */
    public void saveClass(String name) {
        //Changement du nom de la classe
        _class.setName(name);
        //Sauvegarde des attributs et méthodes
        _classPropertiesWindow.saveAttributes();
        _classPropertiesWindow.saveMethods();
        _umlDiagramController.needToSave();
    }

    /**
     * Rafraîchit la classe graphiquement.
     */
    public void refreshGraphics() {
        this._class.computeMinSize();
        _umlDiagramController.refreshGraphics();
    }

}
