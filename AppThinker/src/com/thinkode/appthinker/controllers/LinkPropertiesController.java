package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.models.Link;
import com.thinkode.appthinker.views.LinkPropertiesWindow;

public class LinkPropertiesController {

    private LinkPropertiesWindow _linkPropertiesWindow;
    private UmlDiagramController _umlDiagramController;
    private Link _link;

    /**
     * Constructeur de LinkPropertiesController.
     *
     * @param umlDiagramController Le contrôleur du diagramme UML qui a commandé l'affichage.
     * @param link                 Le lien en cours d'édition.
     */
    public LinkPropertiesController(UmlDiagramController umlDiagramController, Link link) {
        _umlDiagramController = umlDiagramController;
        _link = link;
        _linkPropertiesWindow = new LinkPropertiesWindow();
        _linkPropertiesWindow.setController(this);
        _linkPropertiesWindow.initializeGraphics();
    }

    /**
     * Retourne le nom du lien en cours d'édition.
     *
     * @return Le nom du lien en cours d'édition.
     */
    public String getLinkName() {
        return _link.getName();
    }

    /**
     * Paramètre le nom du lien en cours d'édition.
     *
     * @param name Le nouveau nom.
     */
    public void setLinkName(String name) {
        _link.setName(name);
        _umlDiagramController.needToSave();
    }

    /**
     * Retourne le nom de la classe de départ du lien.
     *
     * @return Le nom de la classe de départ du lien.
     */
    public String getLinkStartName() {
        return _link.getStart().getName();
    }

    /**
     * Retourne le nom de la classe de d'arrivée du lien.
     *
     * @return Le nom de la classe de d'arrivée du lien.
     */
    public String getLinkEndName() {
        return _link.getEnd().getName();
    }

    /**
     * Retourne le type du lien en cours d'édition.
     *
     * @return Le type du lien en cours d'édition.
     */
    public Link.LinkType getLinkType() {
        return _link.getType();
    }

    /**
     * Paramètre le type du lien en cours d'édition.
     *
     * @param type Le nouveau type.
     */
    public void setLinkType(Link.LinkType type) {
        _link.setType(type);
        _umlDiagramController.refreshGraphics();
        _linkPropertiesWindow.fillWindow();
        _umlDiagramController.needToSave();
    }

    /**
     * Retourne le numéro du lien en cours d'édition.
     *
     * @return Le numéro du lien en cours d'édition.
     */
    public int getLinkId() {
        return _link.getId();
    }

    /**
     * Retourne la cardinalité minimum de la classe de départ.
     *
     * @return La cardinalité minimum de la classe de départ.
     */
    public int getLinkMinCardinalityStart() {
        return _link.getMinCardinalityStart();
    }

    /**
     * Retourne la cardinalité maximum de la classe de départ.
     *
     * @return La cardinalité maximum de la classe de départ.
     */
    public int getLinkMaxCardinalityStart() {
        return _link.getMaxCardinalityStart();
    }

    /**
     * Retourne la cardinalité minimum de la classe d'arrivée.
     *
     * @return La cardinalité minimum de la classe d'arrivée.
     */
    public int getLinkMinCardinalityEnd() {
        return _link.getMinCardinalityEnd();
    }

    /**
     * Retourne la cardinalité maximum de la classe d'arrivée.
     *
     * @return La cardinalité maximum de la classe d'arrivée.
     */
    public int getLinkMaxCardinalityEnd() {
        return _link.getMaxCardinalityEnd();
    }

    /**
     * Paramètre la cardinalité minimum de la classe de départ.
     *
     * @param card La cardinalité minimum de la classe de départ.
     */
    public void setLinkMinCardinalityStart(int card) {
        _link.setMinCardinalityStart(card);
        _umlDiagramController.needToSave();
    }

    /**
     * Paramètre la cardinalité maximum de la classe de départ.
     *
     * @param card La cardinalité maximum de la classe de départ.
     */
    public void setLinkMaxCardinalityStart(int card) {
        _link.setMaxCardinalityStart(card);
        _umlDiagramController.needToSave();
    }

    /**
     * Paramètre la cardinalité minimum de la classe d'arrivée.
     *
     * @param card La cardinalité minimum de la classe d'arrivée.
     */
    public void setLinkMinCardinalityEnd(int card) {
        _link.setMinCardinalityEnd(card);
        _umlDiagramController.needToSave();
    }

    /**
     * Paramètre la cardinalité maximum de la classe d'arrivée.
     *
     * @param card La cardinalité maximum de la classe d'arrivée.
     */
    public void setLinkMaxCardinalityEnd(int card) {
        _link.setMaxCardinalityEnd(card);
        _umlDiagramController.needToSave();
    }

    /**
     * Inverse la direction du lien en cours d'édition.
     */
    public void switchDirection() {
        _link.switchDirection();
        _linkPropertiesWindow.save();
        _linkPropertiesWindow.fillWindow();
        _umlDiagramController.needToSave();
    }

    /**
     * Rafraîchit l'affichage.
     */
    public void refresh() {
        //Rafraîchissement du diagramme
        _linkPropertiesWindow.repaint();
        _umlDiagramController.refreshGraphics();
    }

}
