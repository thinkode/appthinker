package com.thinkode.appthinker.controllers;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.models.Composition;
import com.thinkode.appthinker.models.Project;
import com.thinkode.appthinker.models.UmlDiagram;
import com.thinkode.appthinker.views.HomeFrame;
import com.thinkode.appthinker.views.UmlDiagramFrame;
import com.thinkode.appthinker.views.Window;

import javax.swing.*;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URISyntaxException;
import java.net.URL;

public class WindowController {

    private java.util.List<Project> _projects;
    private Window _atWindow;

    /**
     * Constructeur du contrôleur de la vue principale
     */
    public WindowController() {
        _projects = new java.util.ArrayList<Project>();
        _atWindow = new Window();
        _atWindow.setController(this);
        String state = AppThinker.getATProperties().getProperty("showHomeAtStartup");
        if (state.equals("true")) showHomeFrame();
        refreshWorkspace();
    }

    /**
     * Ouvre une composition dans la fenêtre
     *
     * @param projectListId     Le numéro du projet
     * @param compositionListId Le numéro de la composition
     */
    public void openComposition(int projectListId, int compositionListId) {
        Composition composition = _projects.get(projectListId).getCompositions().get(compositionListId);
        if (composition instanceof UmlDiagram) {
            UmlDiagramFrame frame = new UmlDiagramFrame();
            new UmlDiagramController(frame, (UmlDiagram) composition);
            _atWindow.addCompositionFrame(composition.getName(), _projects.get(projectListId).getName(), frame);
        }
    }

    /**
     * Ajoute un nouveau projet vide à la fenêtre en cours
     */
    public void newBlankProject() {
        Project proj = new Project();
        _projects.add(proj);
        _atWindow.setStatusMessage("The blank project has been created.");
        refreshWorkspace();
    }

    /**
     * Ajoute un nouveau projet UML à la fenêtre en cours
     */
    public void newUmlProject() {
        Project proj = new Project();
        //Création d'une composition UML dans le projet
        UmlDiagram umlDiagram = new UmlDiagram();
        proj.addComposition(umlDiagram);
        _projects.add(proj);
        _atWindow.setStatusMessage("The UML project has been created.");
        refreshWorkspace();
    }

    /**
     * Ajoute une composition UML à un projet
     *
     * @param projectListId Le numéro du projet
     */
    public void addUmlComposition(int projectListId) {
        _projects.get(projectListId).addComposition(new UmlDiagram());
        _atWindow.setStatusMessage("The UML Diagram has been added to the project.");
        refreshWorkspace();
    }

    /**
     * Renomme le projet sélectionné
     *
     * @param projectListId Le numéro du projet
     * @param newName       Le nouveau nom
     */
    public void renameProject(int projectListId, String newName) {
        Project project = _projects.get(projectListId);
        if (!projectNameExists(project, newName)) {
            _projects.get(projectListId).setName(newName);
            _atWindow.setStatusMessage("The project has been removed.");
            refreshWorkspace();
        } else {
            _atWindow.showMessage("Another project in the workspace has the same name ! Please choose another name.");
        }
    }

    /**
     * Vérifie si le projet peut être modifié avec ce nom
     *
     * @param project Le projet cible
     * @param name    Le nouveau nom
     */
    private boolean projectNameExists(Project project, String name) {
        for (Project p : _projects) {
            if (p.getName().equals(name) && p != project) return true;
        }
        return false;
    }

    /**
     * Supprime un projet existant
     *
     * @param projectListId Le numéro du projet
     */
    public void deleteProject(int projectListId) {
        Project project = _projects.get(projectListId);
        //Si le projet doit être sauvegardé
        if (askForProjectSaved(project)) {
            int response = _atWindow.showMessage("The project has not yet been saved or contains unsaved compositions. Do you want to save it before closing it ?", "Save before closing", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
            if (response == JOptionPane.OK_OPTION) saveProject(projectListId);
        }
        for (Composition comp : project.getCompositions()) {
            deleteCompositionFrame(project.getName(), comp.getName());
        }
        _projects.remove(projectListId);
        _atWindow.setStatusMessage("The project has been removed from the Workspace.");
        refreshWorkspace();
    }

    /**
     * Renomme la composition d'un projet
     *
     * @param projectListId     Le numéro du projet
     * @param compositionListId Le numéro de la composition
     * @param newName           Le nouveau nom de composition
     */
    public void renameComposition(int projectListId, int compositionListId, String newName) {
        Project project = _projects.get(projectListId);
        Composition comp = project.getCompositions().get(compositionListId);
        if (!compositionNameExists(project, comp, newName)) {
            String oldName = comp.getName();
            comp.setName(newName);
            refreshWorkspace();
            _atWindow.updateCompositionTitle(project.getName(), oldName, newName);
            _atWindow.setStatusMessage("The composition has been renamed.");
        } else
            _atWindow.showMessage("Another composition in the project has the same name ! Please choose another name.");
    }

    /**
     * Vérifie si la composition peut être modifiée avec ce nom
     *
     * @param project     Le projet incluant la composition
     * @param composition La composition cible
     * @param name        Le nouveau nom de la composition.
     */
    private boolean compositionNameExists(Project project, Composition composition, String name) {
        for (Composition c : project.getCompositions()) {
            if (c.getName().equals(name) && c != composition) return true;
        }
        return false;
    }

    /**
     * Supprimer une composition d'un projet
     *
     * @param projectListId     Le numéro du projet
     * @param compositionListId Le numéro de la composition
     */
    public void deleteComposition(int projectListId, int compositionListId) {
        Project project = _projects.get(projectListId);
        Composition comp = project.getCompositions().get(compositionListId);
        project.removeComposition(compositionListId);
        refreshWorkspace();
        deleteCompositionFrame(project.getName(), comp.getName());
        _atWindow.setStatusMessage("The composition has been removed from the project.");
    }

    /**
     * Retire la CompositionFrame du widget
     *
     * @param projectName     Le nom du projet.
     * @param compositionName Le nom de la composition.
     */
    public void deleteCompositionFrame(String projectName, String compositionName) {
        _atWindow.deleteCompositionFrame(projectName, compositionName);
    }

    /**
     * Commande la mise à jour du Workspace
     */
    public void refreshWorkspace() {
        _atWindow.refreshWorkspace(this.getProjects());
    }

    /**
     * Affiche la page d'accueil du logiciel en tant que composition.
     */
    public void showHomeFrame() {
        //Affichage de la page d'accueil
        HomeFrame homeFrame = new HomeFrame();
        new HomeFrameController(homeFrame, this);
        _atWindow.addCompositionFrame("Home page", "AppThinker", homeFrame);
    }

    /**
     * Ouvre un projet depuis un emplacement existant
     *
     * @param path Chemin du projet à ouvrir
     */
    public void openProject(String path) {
        ObjectInputStream ois = null;
        Project project = null;
        try {
            final FileInputStream fichier = new FileInputStream(path);
            ois = new ObjectInputStream(fichier);
            project = (Project) ois.readObject();
        } catch (final java.io.IOException e) {
            e.printStackTrace();
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ois != null) {
                    ois.close();
                    //Ajout du projet à la fenêtre en cours
                    if (!projectAlreadyOpened(project)) {
                        _projects.add(project);
                        refreshWorkspace();
                        _atWindow.setStatusMessage("The project has been opened.");
                    } else {
                        _atWindow.showMessage("An opened project in the workspace has the same name. Please close this project and try again.");
                        _atWindow.setStatusMessage("Unable to open this project.");
                    }
                }
            } catch (final IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Vérifie si le projet est déjà dans le workspace
     *
     * @return Le projet concerné.
     */
    private boolean projectAlreadyOpened(Project project) {
        for (Project p : _projects) {
            if (p.getName().equals(project.getName())) return true;
        }
        return false;
    }

    /**
     * Retourne la liste des projets actuellement ouverts
     *
     * @return Le nom du projet
     */
    public java.util.List<Project> getProjects() {
        return _projects;
    }

    /**
     * Retourne le nom du projet sélectionné dans le Workspace
     *
     * @param projectListId Le numéro du projet.
     * @return Le nom du projet sélectionné.
     */
    public String getProjectName(int projectListId) {
        return _projects.get(projectListId).getName();
    }

    /**
     * Retourne si le projet nécessite d'être sauvegardé ou non
     *
     * @param project Le projet concerné.
     * @return Le projet a besoin d'être sauvegardé.
     */
    public boolean askForProjectSaved(Project project) {
        if (project.getPath() == null || project.isNeededToSave()) return true;
        for (Composition comp : project.getCompositions()) {
            if (comp.isNeededToSave()) return true;
        }
        return false;
    }

    /**
     * Retourne le nom de la composition sélectionnée dans le Workspace
     *
     * @param projectListId     Le numéro du projet
     * @param compositionListId Le numéro de la composition
     * @return Le nom de la composition sélectionnée
     */
    public String getCompositionName(int projectListId, int compositionListId) {
        return _projects.get(projectListId).getCompositions().get(compositionListId).getName();
    }

    /**
     * Sauvegarder le projet
     *
     * @param projectListId Le numéro du projet
     */
    public void saveProject(int projectListId) {
        if (_projects.get(projectListId).saveProject()) {
            _atWindow.setStatusMessage("The project has been saved.");
            refreshWorkspace();
        } else _atWindow.setStatusMessage("Unable to save the project.");
    }

    /**
     * Sauvegarder le projet sous
     *
     * @param projectListId Le numéro du projet
     */
    public void saveAsProject(int projectListId) {
        if (_projects.get(projectListId).saveAsProject()) {
            _atWindow.setStatusMessage("The project has been saved to the specified location.");
            refreshWorkspace();
        } else _atWindow.setStatusMessage("Unable to save the project to the specified location.");
    }

    /**
     * Vérifie si la fenêtre contient des projets non-enregistrés avant la fermeture
     *
     * @return L'autorisation de fermer la fenêtre
     */
    public boolean askForExit() {
        boolean allProjectsSaved = true;
        for (Project p : _projects) {
            if (askForProjectSaved(p)) allProjectsSaved = false;
        }
        System.out.println(allProjectsSaved);
        if (!allProjectsSaved) {
            if (_atWindow.showMessage("The window contains unsaved projects. Do you want to force close ?", "Force closure ?", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE) == JOptionPane.OK_OPTION)
                return true;
            else return false;
        } else return true;
    }

    //Méthodes relatives à la barre de menu

    /**
     * Lance l'ouverture de la fenêtre A Propos
     */
    public void launchAboutWindow() {
        new AboutWindowController();
    }

    /**
     * Lance l'URL du centre de téléchargement du logiciel.
     */
    public void checkForUpdates() {
        try {
            Desktop.getDesktop().browse(new URL("https://insset-my.sharepoint.com/:f:/g/personal/valentin_boulanger_insset_onmicrosoft_com/En4LybbeF2ZMpeSiesh_Af8BX3Fl1aDzTUWyw4dtQzJaag").toURI());
        } catch (URISyntaxException | IOException ex) {
            System.out.println("An error as occur when trying to check for updates : " + ex.toString());
        }
    }
}
