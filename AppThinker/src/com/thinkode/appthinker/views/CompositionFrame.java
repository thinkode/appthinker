package com.thinkode.appthinker.views;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.Serializable;

/**
 * Gère une création qui est ajoutée à un projet
 */
public class CompositionFrame extends JPanel implements MouseListener, MouseMotionListener, Serializable {

    protected CompositionListener _atCompositionListener;

    /**
     * Constructeur - Crée une nouvelle composition.
     */
    public CompositionFrame() {
    }

    /**
     * Ajoute un CompositionListener à la vue
     *
     * @param atCompositionListener La classe d'écoute
     */
    public void addCompositionListener(CompositionListener atCompositionListener) {
        _atCompositionListener = atCompositionListener;
    }

    /**
     * Retire le CompositionListener
     */
    public void removeCompositionListener() {
        _atCompositionListener = null;
    }

    /**
     * Le workspace a besoin de se rafraîchir
     */
    public void needWorkspaceRefresh() {
        _atCompositionListener.refreshWorkspaceNeeded();
    }

    //Evenements souris
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
