package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.controllers.ChangelogWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Fenêtre d'affichage de la version actuelle et des modifications apportées.
 */
public class ChangelogWindow extends JDialog {

    private ChangelogWindowController _atChangelogWindowController;
    private JPanel _panel;

    /**
     * Constructeur de ChangelogWindow
     */
    public ChangelogWindow() {
        //Paramétrage de la fenêtre
        Image img = null;
        try {
            img = ImageIO.read(AppThinker.class.getResource("img/logoAppThinker.png"));
        } catch (Exception ex) {
        }
        this.setModal(true);
        this.setIconImage(img);
        this.setResizable(false);
        this.setLayout(new BorderLayout());
        this.setSize(new Dimension(800, 375));
        this.setLocationRelativeTo(null);
    }

    /**
     * Lie la vue à un contrôleur
     *
     * @param changelogWindowController La classe contrôleur.
     */
    public void setController(ChangelogWindowController changelogWindowController) {
        _atChangelogWindowController = changelogWindowController;
    }

    /**
     * Affiche les informations du changelog dans la fenêtre
     */
    public void displayChangelog() {
        this.setTitle("Version " + _atChangelogWindowController.getVersion());
        _panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g;
                RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g2.setRenderingHints(rh);
                Image img = null;
                try {
                    img = ImageIO.read(AppThinker.class.getResource("img/logoAppThinker.png"));
                } catch (Exception ex) {
                }
                g2.drawImage(img, 0, 20, 300, 300, this);
                //Informations du logiciel/
                g2.setColor(new Color(63, 169, 245));
                g2.setFont(new Font("Arial", Font.BOLD, 40));
                g2.drawString("Thanks for downloading AppThinker !", 50, 45);
                g2.setFont(new Font("Arial", Font.PLAIN, 20));
                g2.drawString("Version " + _atChangelogWindowController.getVersion(), 305, 80);
                g2.setFont(new Font("Arial", Font.PLAIN, 14));
                int i = 140;
                for (String a : _atChangelogWindowController.getChangelog()) {
                    g2.drawString("> " + a, 305, i);
                    i += 25;
                }
            }
        };
        JButton ideas = new JButton("Add your ideas to the software !");
        ideas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _atChangelogWindowController.giveFeedback();
            }
        });
        _panel.add(ideas);
        ideas.setBounds(305, 90, 250, 20);
        _panel.setLayout(null);
        this.getContentPane().add(_panel);
        _panel.repaint();
        this.revalidate();
        this.setVisible(true);
    }
}
