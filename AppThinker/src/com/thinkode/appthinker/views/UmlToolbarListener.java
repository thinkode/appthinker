package com.thinkode.appthinker.views;

public interface UmlToolbarListener {

    /**
     * Evenement appelé lors du changement d'outil de la UmlToolbar
     *
     * @param newTool Le nouvel outil sélectionné.
     */
    void toolChanged(UmlToolbar.UmlTool newTool);

    /**
     * Emet une description de l'outil sélectionné pour affichage dans la statusbar
     *
     * @param description La description de l'outil sélectionné
     */
    void showToolDescription(String description);
}
