package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.controllers.ClassPropertiesController;
import com.thinkode.appthinker.models.Attribute;
import com.thinkode.appthinker.models.Class;
import com.thinkode.appthinker.models.Method;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

/**
 * Classe permettant la création de fenêtres pour la modification des propriétés des classes.
 *
 * @author V.BOULANGER
 */
public class ClassPropertiesWindow extends JDialog {

    private UmlDiagramFrame _umlDiagram;
    private Class _class;
    private JTextField _nameField;
    private JTable _attributesTable;
    private JScrollPane _scrollAttributes;
    private String[] _attributesColumns = {"Name", "Access modifier", "Type", "Static", "Final", "Abstract", "Synchronised", "Volatile", "Transient"};
    private DefaultTableModel _attributeModel;
    private JTable _methodsTable;
    private JScrollPane _scrollMethods;
    private String[] _methodsColumns = {"C", "Name", "Access modifier", "Type", "Arguments", "Static", "Final", "Abstract", "Synchronised", "Volatile", "Transient"};
    private DefaultTableModel _methodModel;
    private JRadioButton _mainRadio;

    private ClassPropertiesController _classPropertiesController;

    /**
     * Constructeur - Crée une instance de la fenêtre de propriétés de classe à partir d'un diagramme et de la classe à modifier.
     */
    public ClassPropertiesWindow() {
    }

    /**
     * Charge les informations dans la fenêtre
     */
    public void initializeGraphics() {
        //Paramétrage de la fenêtre
        this.setTitle("Edit properties - " + _classPropertiesController.getClassName());
        this.setModal(true);
        this.setSize(new Dimension(800, 375));
        Image img = null;
        try {
            img = ImageIO.read(AppThinker.class.getResource("../../com.thinkode.appthinker.img/logoAppThinker.png"));
        } catch (Exception ex) {
        }
        this.setIconImage(img);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());

        //Espace général de la fenêtre
        JPanel generalPanel = new JPanel();
        generalPanel.setLayout(new BoxLayout(generalPanel, BoxLayout.Y_AXIS));

        //Espace de modification du nom
        JPanel namePan = new JPanel();
        namePan.setLayout(new BoxLayout(namePan, BoxLayout.X_AXIS));
        JLabel nameLbl = new JLabel("Name : ");
        _nameField = new JTextField();
        _nameField.setText(_classPropertiesController.getClassName());
        _nameField.setPreferredSize(new Dimension(300, 20));
        namePan.add(nameLbl);
        namePan.add(_nameField);
        generalPanel.add(namePan);

        //Radio bouton pour définir la classe principale
        _mainRadio = new JRadioButton("This is the main class");
        _mainRadio.setToolTipText("The main class is the entry point of the application. It appears in red on the diagram.");
        _mainRadio.setSelected(_classPropertiesController.isMainClass());
        _mainRadio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.setMainClass();
            }
        });
        ButtonGroup bg = new ButtonGroup();
        bg.add(_mainRadio);

        generalPanel.add(_mainRadio);

        JLabel attrLbl = new JLabel("Edit attributes");
        generalPanel.add(attrLbl);

        //Espace de modification des attributs
        JPanel attributesPan = new JPanel();
        attributesPan.setLayout(new BoxLayout(attributesPan, BoxLayout.X_AXIS));

        _attributeModel = new DefaultTableModel(_attributesColumns, 0);
        _attributesTable = new JTable(_attributeModel);

        _scrollAttributes = new JScrollPane(_attributesTable);
        _scrollAttributes.setPreferredSize(new Dimension(350, 100));
        attributesPan.add(_scrollAttributes);

        JPanel attributesTableModifier = new JPanel();
        attributesTableModifier.setLayout(new BoxLayout(attributesTableModifier, BoxLayout.Y_AXIS));
        JButton addAttribute = new JButton(" + ");
        addAttribute.setToolTipText("Add a new attribute.");
        addAttribute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.addAttribute();
            }
        });
        attributesTableModifier.add(addAttribute);
        JButton removeAttribute = new JButton(" -  ");
        removeAttribute.setToolTipText("Remove the selected attribute.");
        removeAttribute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.removeAttribute(_attributesTable.getSelectedRow());
            }
        });
        attributesTableModifier.add(removeAttribute);
        JButton upAttribute = new JButton("▲");
        upAttribute.setToolTipText("Go up the selected attribute.");
        upAttribute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.goUpAttribute(_attributesTable.getSelectedRow());
            }
        });
        attributesTableModifier.add(upAttribute);
        JButton downAttribute = new JButton("▼");
        downAttribute.setToolTipText("Go down the selected attribute.");
        downAttribute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.goDownAttribute(_attributesTable.getSelectedRow());
            }
        });
        attributesTableModifier.add(downAttribute);
        attributesPan.add(attributesTableModifier);

        generalPanel.add(attributesPan);

        JLabel methLbl = new JLabel("Edit methods");
        generalPanel.add(methLbl);

        //Espace de modification des méthodes
        JPanel methodsPan = new JPanel();
        methodsPan.setLayout(new BoxLayout(methodsPan, BoxLayout.X_AXIS));

        _methodModel = new DefaultTableModel(_methodsColumns, 0);
        _methodsTable = new JTable(_methodModel);

        _methodsTable.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Point p = e.getPoint();
                int col = _methodsTable.columnAtPoint(p);
                int row = _methodsTable.rowAtPoint(p);
                if (col == 4) _classPropertiesController.openArgumentsWindow(_methodsTable.getSelectedRow());
            }
        });

        _scrollMethods = new JScrollPane(_methodsTable);
        _scrollMethods.setPreferredSize(new Dimension(350, 100));
        methodsPan.add(_scrollMethods);

        JPanel methodsTableModifier = new JPanel();
        methodsTableModifier.setLayout(new BoxLayout(methodsTableModifier, BoxLayout.Y_AXIS));
        JButton addMethod = new JButton(" + ");
        addMethod.setToolTipText("Add a new method.");
        addMethod.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.addMethod();
            }
        });
        methodsTableModifier.add(addMethod);

        JButton removeMethod = new JButton(" -  ");
        removeMethod.setToolTipText("Remove the selected method.");
        removeMethod.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.removeMethod(_methodsTable.getSelectedRow());
            }
        });
        methodsTableModifier.add(removeMethod);
        JButton upMethod = new JButton("▲");
        upMethod.setToolTipText("Go up the selected method.");
        upMethod.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.goUpMethod(_methodsTable.getSelectedRow());
            }
        });
        methodsTableModifier.add(upMethod);
        JButton downMethod = new JButton("▼");
        downMethod.setToolTipText("Go down the selected method.");
        downMethod.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.goDownMethod(_methodsTable.getSelectedRow());
            }
        });
        methodsTableModifier.add(downMethod);
        JButton overloadMethod = new JButton(" O ");
        overloadMethod.setToolTipText("Overload this method.");
        overloadMethod.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _classPropertiesController.overloadMethod(_methodsTable.getSelectedRow());
            }
        });
        methodsTableModifier.add(overloadMethod);

        methodsPan.add(methodsTableModifier);

        generalPanel.add(methodsPan);

        this.add(generalPanel, BorderLayout.CENTER);

        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            //On enregistre à la fermeture de la fenêtre
            @Override
            public void windowClosing(WindowEvent e) {
                _classPropertiesController.saveClass(_nameField.getText());
                _classPropertiesController.refreshGraphics();
                dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });

        _classPropertiesController.refreshAttributes();
        _classPropertiesController.refreshMethods();
        this.setVisible(true);
    }

    /**
     * Lie la vue à un contrôleur
     *
     * @param classPropertiesController La classe contrôleur
     */
    public void setController(ClassPropertiesController classPropertiesController) {
        _classPropertiesController = classPropertiesController;
    }

    /**
     * Sélectionne une ligne dans le tableau d'attributs
     *
     * @param index L'index de la ligne à sélectionner
     */
    public void selectAttribute(int index) {
        _attributesTable.setRowSelectionInterval(index, index);
    }

    /**
     * Sélectionne une ligne dans le tableau des méthodes
     *
     * @param index L'index de la ligne à sélectionner
     */
    public void selectMethod(int index) {
        _methodsTable.setRowSelectionInterval(index, index);
    }

    /**
     * Affiche la liste des attributs de la classe dans le tableau
     *
     * @param attributes La liste des attributs
     */
    public void listAttributes(java.util.List<Attribute> attributes) {
        //Import des attributs dans la table
        _attributeModel.setDataVector((Object[][]) null, _attributesColumns);
        for (Attribute attr : attributes) {
            String access = (attr.getAccess() == "+") ? "PUBLIC" : (attr.getAccess() == "#") ? "PROTECTED" : "PRIVATE";
            _attributeModel.addRow(new Object[]{attr.getName(), access, attr.getType(), attr.isStatic(), attr.isFinal(), attr.isAbstract(), attr.isSynchronized(), attr.isVolatile(), attr.isTransient()});
        }
        //On ajoute les contrôles pour chaque colonne
        String[] access = {"PRIVATE", "PUBLIC", "PROTECTED"};
        JComboBox accessComboBox = new JComboBox(access);
        accessComboBox.setEditable(true);
        _attributesTable.getColumn(_attributesColumns[1]).setCellEditor(new DefaultCellEditor(accessComboBox));
        String[] types = {"boolean", "char", "byte", "short", "int", "long", "float", "double", "String"};
        JComboBox typeComboBox = new JComboBox(types);
        typeComboBox.setEditable(true);
        _attributesTable.getColumn(_attributesColumns[2]).setCellEditor(new DefaultCellEditor(typeComboBox));
        _attributesTable.getColumn(_attributesColumns[3]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _attributesTable.getColumn(_attributesColumns[4]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _attributesTable.getColumn(_attributesColumns[5]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _attributesTable.getColumn(_attributesColumns[6]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _attributesTable.getColumn(_attributesColumns[7]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _attributesTable.getColumn(_attributesColumns[8]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
    }

    /**
     * Affiche la liste des méthodes de la classe dans le tableau.
     *
     * @param methods La liste des méthodes
     */
    public void listMethods(java.util.List<Method> methods) {
        //Import des méthodes dans la table
        _methodModel.setDataVector((Object[][]) null, _methodsColumns);
        for (Method meth : methods) {
            String access = (meth.getAccess() == "-") ? "PRIVATE" : (meth.getAccess() == "#") ? "PROTECTED" : "PUBLIC";
            _methodModel.addRow(new Object[]{meth.isConstructor(), meth.getName(), access, meth.getType(), "[EDIT]", meth.isStatic(), meth.isFinal(), meth.isAbstract(), meth.isSynchronized(), meth.isVolatile(), meth.isTransient()});
        }

        //On ajoute les contrôles pour chaque colonne
        String[] access = {"PRIVATE", "PUBLIC", "PROTECTED"};
        JComboBox accessComboBox = new JComboBox(access);
        accessComboBox.setEditable(true);
        _methodsTable.getColumn(_methodsColumns[0]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _methodsTable.getColumn(_methodsColumns[2]).setCellEditor(new DefaultCellEditor(accessComboBox));
        String[] types = {"boolean", "char", "byte", "short", "int", "long", "float", "double", "String"};
        JComboBox typeComboBox = new JComboBox(types);
        typeComboBox.setEditable(true);
        _methodsTable.getColumn(_methodsColumns[3]).setCellEditor(new DefaultCellEditor(typeComboBox));
        JTextField argsField = new JTextField();
        argsField.setEnabled(false);
        _methodsTable.getColumn(_methodsColumns[4]).setCellEditor(new DefaultCellEditor(argsField));
        _methodsTable.getColumn(_methodsColumns[5]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _methodsTable.getColumn(_methodsColumns[6]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _methodsTable.getColumn(_methodsColumns[7]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _methodsTable.getColumn(_methodsColumns[8]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _methodsTable.getColumn(_methodsColumns[9]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
        _methodsTable.getColumn(_methodsColumns[10]).setCellEditor(new DefaultCellEditor(new JCheckBox()));
    }

    /**
     * Sauvegarde les attributs pour la classe en cours.
     */
    public void saveAttributes() {
        //Enregistrement des attributs
        _attributesTable.editCellAt(0, 0);
        for (int i = 0; i <= _attributeModel.getRowCount() - 1; i++) {
            Vector vect = (Vector) _attributeModel.getDataVector().elementAt(i);
            String access = vect.get(1).toString();
            _classPropertiesController.saveAttribute(i,
                    vect.get(0).toString(),
                    (access == "PUBLIC") ? Attribute.PUBLIC : (access == "PROTECTED") ? Attribute.PROTECTED : Attribute.PRIVATE,
                    vect.get(2).toString(),
                    (boolean) vect.get(3),
                    (boolean) vect.get(4),
                    (boolean) vect.get(5),
                    (boolean) vect.get(6),
                    (boolean) vect.get(7),
                    (boolean) vect.get(8));
        }
    }

    /**
     * Sauvegarde les méthodes pour la classe en cours.
     */
    public void saveMethods() {
        //Enregistrement des méthodes
        _methodsTable.editCellAt(0, 0);
        for (int i = 0; i <= _methodModel.getRowCount() - 1; i++) {
            Vector vect = (Vector) _methodModel.getDataVector().elementAt(i);
            String access = vect.get(2).toString();
            _classPropertiesController.saveMethod(i,
                    (boolean) vect.get(0),
                    vect.get(1).toString(),
                    (access == "PRIVATE") ? Method.PRIVATE : (access == "PROTECTED") ? Method.PROTECTED : Method.PUBLIC,
                    vect.get(3).toString(),
                    (boolean) vect.get(5),
                    (boolean) vect.get(6),
                    (boolean) vect.get(7),
                    (boolean) vect.get(8),
                    (boolean) vect.get(9),
                    (boolean) vect.get(10));
        }
    }
}
