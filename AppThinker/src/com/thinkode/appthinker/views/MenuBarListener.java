package com.thinkode.appthinker.views;

public interface MenuBarListener {
    /**
     * Evenement appelé lors d'un clic sur le bouton Nouveau Projet Vide
     */
    void newBlankProjectClicked();

    /**
     * Evenement appelé lors d'un clic sur le bouton Nouveau Projet UML
     */
    void newUmlProjectClicked();

    /**
     * Evenement appelé lors d'un clic sur le bouton Ouvrir un projet
     */
    void openProjectClicked();

    /**
     * Evenement appelé lors d'un clic sur le bouton Quitter
     */
    void quitClicked();

    /**
     * Evenement appelé lors d'un clic sur le bouton A propos d'AppThinker
     */
    void aboutAppThinkerClicked();

    /**
     * Evenement appelé lors d'un clic sur le bouton A propos d'AppThinker
     */
    void checkUpdatesClicked();
}
