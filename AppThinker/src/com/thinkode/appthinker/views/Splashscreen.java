package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

/**
 * Ecran de démarrage du logiciel
 */
public class Splashscreen extends JFrame {

    /**
     * Constructeur - Crée la fenêtre de démarrage
     */
    public Splashscreen() {
        this.setTitle("AppThinker - Starting");
        int sizeX = 600;
        int sizeY = 350;
        this.setMinimumSize(new Dimension(sizeX, sizeY));
        Image img = null;
        try {
            img = ImageIO.read(AppThinker.class.getResource("/com/thinkode/appthinker/img/logoAppThinker.png"));
        } catch (Exception ex) {
        }
        this.setIconImage(img);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);

        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g;
                RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g2.setRenderingHints(rh);
                Image img = null;
                try {
                    img = ImageIO.read(AppThinker.class.getResource("/com/thinkode/appthinker/img/splashscreen.png"));
                } catch (Exception ex) {
                }
                g2.drawImage(img, 0, 0, sizeX, sizeY, this);
                /*Informations du logiciel*/
                g2.setColor(new Color(63, 169, 245));
                g2.setFont(new Font("Arial", Font.BOLD, 40));
                g2.drawString("AppThinker", sizeX - 250, 45);
                g2.setFont(new Font("Arial", Font.PLAIN, 20));
                g2.drawString("Make your ideas come true", sizeX - 250, sizeY - 20);
                g2.setFont(new Font("Arial", Font.BOLD, 10));
                g2.drawString("We're getting things ready...", 5, sizeY - 5);
                g2.drawString(AppThinker.developer + " © 2021 - Version " + AppThinker.version, sizeX - 250, sizeY - 5);
            }
        };

        this.getContentPane().add(panel);
        this.setVisible(true);
    }
}
