package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.controllers.AboutWindowController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Fenêtre contenant les informations du logiciel
 */
public class AboutWindow extends JDialog {

    private AboutWindowController _atAboutController;

    /**
     * Constructeur - Crée une fenêtre A propos
     */
    public AboutWindow() {
    }

    /**
     * Charge les informations dans la fenêtre
     */
    public void initializeGraphics() {
        //Paramétrage de la fenêtre
        this.setTitle("AppThinker");
        this.setModal(true);
        this.setSize(new Dimension(800, 375));
        Image img = null;
        try {
            img = ImageIO.read(AppThinker.class.getResource("img/logoAppThinker.png"));
        } catch (Exception ex) {
        }
        this.setIconImage(img);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());

        JPanel panel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g;
                RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g2.setRenderingHints(rh);
                Image img = null;
                try {
                    img = ImageIO.read(AppThinker.class.getResource("img/logoAppThinker.png"));
                } catch (Exception ex) {
                }
                g2.drawImage(img, 0, 20, 300, 300, this);
                /*Informations du logiciel*/
                g2.setColor(new Color(63, 169, 245));
                g2.setFont(new Font("Arial", Font.BOLD, 40));
                g2.drawString("AppThinker", 305, 45);
                g2.setFont(new Font("Arial", Font.PLAIN, 20));
                g2.drawString("Make your ideas come true", 305, 80);
                g2.setFont(new Font("Arial", Font.PLAIN, 14));
                g2.drawString("> Version : " + AppThinker.version, 305, 140);
                g2.drawString("> Editing date : " + AppThinker.dateVersion, 305, 165);
                g2.drawString("> Developer : " + AppThinker.developer, 305, 190);
                g2.drawString(">", 305, 215);
                g2.drawString(">", 305, 240);
                g2.drawString(">", 305, 265);
                g2.drawString(">", 305, 290);
            }
        };

        panel.setLayout(null);
        JButton updates = new JButton("Check for updates");
        updates.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _atAboutController.checkForUpdates();
            }
        });
        panel.add(updates);
        updates.setBounds(320, 200, 250, 20);

        JButton changelog = new JButton("View Changelog");
        changelog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _atAboutController.launchChangelogWindow();
            }
        });
        panel.add(changelog);
        changelog.setBounds(320, 225, 250, 20);

        JButton ideas = new JButton("Add your ideas to the software !");
        ideas.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _atAboutController.giveFeedback();
            }
        });
        panel.add(ideas);
        ideas.setBounds(320, 250, 250, 20);

        JButton donation = new JButton("Make a donation - Thanks !");
        donation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _atAboutController.makeDonation();
            }
        });
        panel.add(donation);
        donation.setBounds(320, 275, 250, 20);

        this.getContentPane().add(panel);
        this.setVisible(true);
    }

    /**
     * Lie la vue à un contrôleur A propos
     *
     * @param atAboutController Contrôleur de la fenêtre A propos
     */
    public void setController(AboutWindowController atAboutController) {
        _atAboutController = atAboutController;
    }
}
