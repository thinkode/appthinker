package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Affiche une barre de menu en haut de la fenêtre.
 *
 * @author V.BOULANGER
 */
public class MenuBar extends JMenuBar {

    private JMenu _fileMenu;
    private JMenu _newProject;
    private JMenuItem _newBlankProject;
    private JMenuItem _newUmlProject;
    private JMenuItem _openProject;
    private JMenuItem _saveProject;
    private JMenuItem _saveAsProject;
    private JMenuItem _projectOptions;
    private JMenuItem _closeProject;
    private JMenuItem _quitMenu;

    private JMenu _appMenu;
    private JMenuItem _appSettings;
    private JMenuItem _appInfo;
    private JMenuItem _appChangelog;

    private MenuBarListener _menuBarListener;

    /**
     * Constructeur de la classe MenuBar
     */
    public MenuBar() {
        //Création de la barre menu
        _fileMenu = new JMenu("File");

        _newProject = new JMenu("New");
        _newProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/newProject.png")));
        _newBlankProject = new JMenuItem("Blank project");
        _newBlankProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/blankProject.png")));
        _newBlankProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuBarListener.newBlankProjectClicked();
            }
        });
        _newProject.add(_newBlankProject);
        _newUmlProject = new JMenuItem("UML project");
        _newUmlProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/umlProject.png")));
        _newUmlProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuBarListener.newUmlProjectClicked();
            }
        });
        _newProject.add(_newUmlProject);
        _fileMenu.add(_newProject);
        _openProject = new JMenuItem("Open project");
        _openProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/importProject.png")));
        _openProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuBarListener.openProjectClicked();
            }
        });
        _fileMenu.add(_openProject);
        _quitMenu = new JMenuItem("Quit");
        _quitMenu.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/quit.png")));
        _quitMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuBarListener.quitClicked();
            }
        });
        _fileMenu.add(_quitMenu);

        this.add(_fileMenu);

        _appMenu = new JMenu("AppThinker");

        _appSettings = new JMenuItem("Settings");
        _appSettings.setEnabled(false);
        _appSettings.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/settings.png")));
        _appMenu.add(_appSettings);
        _appInfo = new JMenuItem("About AppThinker");
        _appInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuBarListener.aboutAppThinkerClicked();
            }
        });
        _appInfo.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/info.png")));
        _appMenu.add(_appInfo);
        _appChangelog = new JMenuItem("Check for updates");
        _appChangelog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _menuBarListener.checkUpdatesClicked();
            }
        });
        _appChangelog.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/news.png")));
        _appMenu.add(_appChangelog);

        this.add(_appMenu);
    }

    /**
     * Ajoute un écouteur d'événement
     *
     * @param listener La classe écouteur
     */
    public void addMenuBarListener(MenuBarListener listener) {
        _menuBarListener = listener;
    }
}
