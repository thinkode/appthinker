package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.controllers.WindowController;
import com.thinkode.appthinker.models.Project;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Affiche une fenêtre du logiciel.
 */
public class Window extends JFrame implements MenuBarListener, WorkspaceListener, CompositionWidgetListener {

    private final MenuBar _menubar;
    private final Statusbar _statusbar;
    private final Workspace _workspace;
    private CompositionWidget _compositionWidget;

    private WindowController _mainWindowController;

    /**
     * Constructeur de la classe AppThinkerWindow
     */
    public Window() {
        //Paramétrage de la fenêtre
        this.setTitle("AppThinker");
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setMinimumSize(new Dimension(1000, 500));
        Image img;
        try {
            img = ImageIO.read(AppThinker.class.getResource("img/logoAppThinker.png"));
        } catch (Exception ex) {
            throw new NotImplementedException();
        }
        this.setIconImage(img);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                quitClicked();
            }
        });
        this.setLocationRelativeTo(null);

        this.setLayout(new BorderLayout());

        //Ajout du menu à la fenêtre
        _menubar = new MenuBar();
        _menubar.addMenuBarListener(this);
        this.setJMenuBar(_menubar);

        //Ajout de la statusbar à la fenêtre
        _statusbar = new Statusbar();
        this.add(_statusbar, BorderLayout.SOUTH);

        //Ajout du Workspace à la fenêtre
        _workspace = new Workspace();
        _workspace.addWorkspaceListener(this);
        this.add(_workspace, BorderLayout.WEST);

        //Ajout du Widget de visualisation des compositions
        _compositionWidget = new CompositionWidget();
        _compositionWidget.addCompositionWidgetListener(this);
        this.add(_compositionWidget, BorderLayout.CENTER);
        this.setVisible(true);
    }

    /**
     * Lie un contrôleur de fenêtre principale à la vue
     *
     * @param mainWindowController Un contrôleur de la fenêtre principale
     */
    public void setController(WindowController mainWindowController) {
        _mainWindowController = mainWindowController;
    }

    /**
     * Ouvre un projet existant dans la fenêtre.
     */
    public void openProject() {
        FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("AppThinker project", "appt");
        JFileChooser dialog = new JFileChooser();
        dialog.setDialogType(JFileChooser.OPEN_DIALOG);
        dialog.setDialogTitle("Open an AppThinker project");
        dialog.setFileFilter(fileFilter);
        dialog.setAcceptAllFileFilterUsed(false);
        if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            _mainWindowController.openProject(dialog.getSelectedFile().getPath());
        }
    }

    /**
     * Récupère la menubar contenue dans la fenêtre.
     *
     * @return La menubar contenue dans la fenêtre.
     */
    public MenuBar getMenubar() {
        return this._menubar;
    }

    /**
     * Récupère la statusbar contenue dans la fenêtre.
     *
     * @return La statusbar contenue dans la fenêtre.
     */
    public Statusbar getStatusbar() {
        return this._statusbar;
    }

    /**
     * Ajoute une composition dans le widget central.
     *
     * @param composition     La composition à afficher.
     * @param projectName     Le nom du projet
     * @param compositionName La vue de la composition
     */
    public void addCompositionFrame(String compositionName, String projectName, CompositionFrame composition) {
        //Ajout de la composition au widget central
        _compositionWidget.addCompositionFrame(compositionName, projectName, composition);
        this.revalidate();
    }

    /**
     * Affiche un message d'alerte
     *
     * @param message Le message à afficher.
     */
    public void showMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }

    /**
     * Affiche une boîte de dialogue
     *
     * @param message Le message à afficher
     * @param title   Le titre de la boite de dialogue
     * @param option  Les choix disponibles
     * @param type    Le type d'alerte
     * @return Un entier correspondant au choix de l'utilisateur
     */
    public int showMessage(String message, String title, int option, int type) {
        return JOptionPane.showOptionDialog(this, message, title, option, type, null, null, null);
    }

    /**
     * Affiche un message d'erreur dans la barre de statut
     *
     * @param message Le message à afficher
     */
    public void setStatusMessage(String message) {
        _statusbar.setStatusMessage(message);
    }

    /**
     * Remet à jour le Workspace à l'aide des projets de la fenêtre
     *
     * @param projects La liste de projets
     */
    public void refreshWorkspace(java.util.List<Project> projects) {
        _workspace.refreshTree(projects);
    }

    /**
     * Remet à jour le titre de la composition ouverte dans le CompositionWidget
     *
     * @param project Le projet contenant la composition
     * @param oldName L'ancien nom de la composition
     * @param newName Le nouveau nom de la composition
     */
    public void updateCompositionTitle(String project, String oldName, String newName) {
        _compositionWidget.updateCompositionTitle(project, oldName, newName);
    }

    /**
     * Supprime la composition du CompositionWidget
     *
     * @param project     Le projet contenant la composition à supprimer
     * @param composition La composition à supprimer
     */
    public void deleteCompositionFrame(String project, String composition) {
        _compositionWidget.deleteCompositionFrame(project, composition);
    }

    //Evenements du Workspace

    /**
     * Page d'accueil cliquée
     */
    @Override
    public void homePageClicked() {
        _mainWindowController.showHomeFrame();
    }

    /**
     * Ajout d'une composition UML
     *
     * @param projectListId Le numéro de projet
     */
    @Override
    public void addUmlComposition(int projectListId) {
        _mainWindowController.addUmlComposition(projectListId);
    }

    /**
     * Retourne le nom du projet
     *
     * @param projectListId Le numéro du projet
     * @return Le nom du projet
     */
    @Override
    public String askForProjectName(int projectListId) {
        return _mainWindowController.getProjectName(projectListId);
    }

    /**
     * Retourne si le projet a besoin d'être sauvegardé au non
     *
     * @param project Le projet concerné
     * @return Un booléen représentant l'affirmation.
     */
    @Override
    public boolean askForProjectSaved(Project project) {
        return _mainWindowController.askForProjectSaved(project);
    }

    /**
     * Renomme le projet
     *
     * @param projectListId Le numéro de projet concerné
     * @param newName       Le nouveau nom
     */
    @Override
    public void renameProject(int projectListId, String newName) {
        _mainWindowController.renameProject(projectListId, newName);
    }

    /**
     * Sauvegarde du projet
     *
     * @param projectListId Le numéro du projet concerné
     */
    @Override
    public void saveProjectClicked(int projectListId) {
        _mainWindowController.saveProject(projectListId);
    }

    /**
     * Sauvegarde du projet sous...
     *
     * @param projectListId Le numéro du projet concerné
     */
    @Override
    public void saveAsProjectClicked(int projectListId) {
        _mainWindowController.saveAsProject(projectListId);
    }

    /**
     * Suppression du projet
     *
     * @param projectListId Le numéro du projet concerné
     */
    @Override
    public void deleteProject(int projectListId) {
        _mainWindowController.deleteProject(projectListId);
    }

    /**
     * Retourne le nom de la composition
     *
     * @param projectListId     Le numéro de projet
     * @param compositionListId Le numéro de la composition
     * @return Le nom de la composition
     */
    @Override
    public String askForCompositionName(int projectListId, int compositionListId) {
        return _mainWindowController.getCompositionName(projectListId, compositionListId);
    }

    /**
     * Renomme la composition
     *
     * @param projectListId     Le numéro de projet
     * @param compositionListId Le numéro de la composition
     * @param newName           Le nouveau nom
     */
    @Override
    public void renameComposition(int projectListId, int compositionListId, String newName) {
        _mainWindowController.renameComposition(projectListId, compositionListId, newName);
    }

    /**
     * Suppression de la composition
     *
     * @param projectListId     Le numéro du projet
     * @param compositionListId Le numéro de la composition
     */
    @Override
    public void deleteComposition(int projectListId, int compositionListId) {
        _mainWindowController.deleteComposition(projectListId, compositionListId);
    }

    //Evènements de la barre de menu (MenuBarListener)

    /**
     * Bouton Nouveau projet cliqué
     */
    @Override
    public void newBlankProjectClicked() {
        _mainWindowController.newBlankProject();
    }

    /**
     * Double-clic sur une composition
     *
     * @param projectListId     Le numéro de projet
     * @param compositionListId Le numéro de composition
     */
    @Override
    public void compositionDoubleClick(int projectListId, int compositionListId) {
        _mainWindowController.openComposition(projectListId, compositionListId);
    }

    /**
     * Bouton nouveau projet cliqué
     */
    @Override
    public void newUmlProjectClicked() {
        _mainWindowController.newUmlProject();
    }

    /**
     * Bouton Ouvrir projet cliqué
     */
    @Override
    public void openProjectClicked() {
        openProject();
    }

    /**
     * Demande de la fermeture de la fenêtre
     */
    @Override
    public void quitClicked() {
        if (_mainWindowController.askForExit()) System.exit(0);
    }

    /**
     * Clic sur le bouton A Propos
     */
    @Override
    public void aboutAppThinkerClicked() {
        _mainWindowController.launchAboutWindow();
    }

    /**
     * Clic sur le bouton de mise à jour
     */
    @Override
    public void checkUpdatesClicked() {
        _mainWindowController.checkForUpdates();
    }

    //Evenement du CompositionWidget

    /**
     * Demande la mise à jour du Workspace
     */
    @java.lang.Override
    public void refreshWorkspaceNeeded() {
        _mainWindowController.refreshWorkspace();
    }

    /**
     * Demande l'affichage d'un message dans la barre de statut
     *
     * @param message Le message à afficher
     */
    @Override
    public void statusEmitted(String message) {
        setStatusMessage(message);
    }
}
