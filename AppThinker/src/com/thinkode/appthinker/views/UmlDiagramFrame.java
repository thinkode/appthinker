package com.thinkode.appthinker.views;

import com.thinkode.appthinker.controllers.UmlDiagramController;
import com.thinkode.appthinker.models.Class;
import com.thinkode.appthinker.models.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Cette classe permet d'afficher les éléments UML de la composition.
 */
public class UmlDiagramFrame extends CompositionFrame implements UmlToolbarListener, Serializable {

    private UmlToolbar _umlToolbar;
    private Object _selected;
    private Object _hovered;
    private ClassGrip _gripHovered = null;
    private ClassGrip _gripSelected = null;
    private boolean _viewGrips = false;
    private int gripSize = 8;

    private int _sizeX = 3000;
    private int _sizeY = 3000;

    private UmlDiagramController _umlDiagramController;

    public enum ClassGrip {
        GRIP_N,
        GRIP_NE,
        GRIP_E,
        GRIP_SE,
        GRIP_S,
        GRIP_SW,
        GRIP_W,
        GRIP_NW
    }

    //Type de liens
    final static float dash[] = {5.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
    private int _shiftX;
    private int _shiftY;

    private JPanel _drawPanel;

    /**
     * Constructeur - Crée un nouveau diagramme UML à partir d'un projet.
     */
    public UmlDiagramFrame() {
        super();
        this.setLayout(new BorderLayout());

        _sizeX = 3000;
        _sizeY = 3000;

        _umlToolbar = new UmlToolbar();
        _umlToolbar.addUmlToolbarListener(this);
        this.add(_umlToolbar, BorderLayout.NORTH);
        _umlToolbar.setEnabled(true);

        initGraphics();
        redraw();
    }

    public void setController(UmlDiagramController umlDiagramController) {
        _umlDiagramController = umlDiagramController;
    }

    /**
     * Initialise la fonction pour redessiner le panel
     */
    public void initGraphics() {
        _drawPanel = new JPanel() {
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g;
                //Activation de l'antialiasing pour les dessins
                RenderingHints rh = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g2.setRenderingHints(rh);

                Font font1 = new Font("Montserrat Light", Font.PLAIN, 14);
                FontMetrics metrics1 = _drawPanel.getFontMetrics(font1);
                Font font2 = new Font("Montserrat Light", Font.PLAIN, 10);
                FontMetrics metrics2 = _drawPanel.getFontMetrics(font2);

                g2.setColor(new Color(127, 158, 178));
                g2.drawString(_umlDiagramController.getName(), 10, 20);
                for (Class a : _umlDiagramController.getClassesList()) {
                    g2.setFont(font1);
                    int posX = a.getPosX() - (a.getSizeX() / 2);
                    int posY = a.getPosY() - (a.getSizeY() / 2);
                    //Dessin du rectangle
                    g2.setColor(new Color(127, 158, 178));
                    g2.fillRect(posX, posY, a.getSizeX(), a.getSizeY());
                    if (a == _umlDiagramController.getMainClass()) {
                        g2.setColor(new Color(173, 37, 8));
                        g2.drawRect(posX, posY, a.getSizeX(), a.getSizeY());
                    }
                    g2.setColor(Color.BLACK);
                    //Dessin du nom de la classe
                    g2.setColor(new Color(39, 76, 94));
                    int posCounter = posY + font1.getSize();
                    g2.drawString(a.getName(), posX + a.getSizeX() / 2 - metrics1.stringWidth(a.getName()) / 2, posCounter);
                    posCounter += 5;
                    g2.setColor(new Color(218, 233, 244));
                    //Ligne de séparation
                    g2.drawLine(posX, posY + font1.getSize() + 5, posX + a.getSizeX() - 1, posY + font1.getSize() + 5);
                    g2.setFont(font2);
                    posCounter += font2.getSize();
                    g2.drawString("attributes", posX + a.getSizeX() / 2 - metrics2.stringWidth("attributes") / 2, posCounter);
                    //Affichage des attributs
                    g2.setColor(new Color(39, 76, 94));
                    for (Attribute b : a.getAttributes()) {
                        posCounter += font2.getSize();
                        g2.drawString(b.getAccess() + " " + b.getName() + " : " + b.getType(), posX + 5, posCounter);
                        //Si l'attribut est statique, on le souligne
                        if (b.isStatic())
                            g2.drawLine(a.getPosX() - a.getSizeX() / 2 + 10, posCounter + 1, a.getPosX() + a.getSizeX() / 2 - 10, posCounter + 1);
                    }
                    posCounter += 5;
                    g2.setColor(new Color(218, 233, 244));
                    //Ligne de séparation
                    g2.drawLine(posX, posCounter, posX + a.getSizeX() - 1, posCounter);
                    posCounter += font2.getSize();
                    g2.drawString("methods", posX + a.getSizeX() / 2 - metrics2.stringWidth("methods") / 2, posCounter);
                    //Dessin des méthodes
                    for (Method m : a.getMethods()) {
                        posCounter += font2.getSize();
                        //Si la méthode est un constructeur, on l'affiche en rouge, sinon en bleu
                        System.out.println(a.getName());
                        if (m.isConstructor()) g2.setColor(new Color(187, 11, 11));
                        else g2.setColor(new Color(39, 76, 94));
                        String chain = m.getAccess() + " " + m.getName() + "(";
                        ArrayList<String> listArguments = new ArrayList<String>();
                        for (Argument ar : m.getArguments()) {
                            listArguments.add(ar.getName() + " : " + ar.getType());
                        }
                        chain += String.join(", ", listArguments) + ") : " + m.getType();
                        g2.drawString(chain, posX + 5, posCounter);
                        //Si l'attribut est statique, on le souligne
                        if (m.isStatic())
                            g2.drawLine(a.getPosX() - a.getSizeX() / 2 + 10, posCounter + 1, a.getPosX() + a.getSizeX() / 2 - 10, posCounter + 1);
                    }
                    //Affichage du lien temporaire rouge
                    if (_selected != null && _selected instanceof Class && _gripSelected != null) {
                        Class b = (Class) _selected;
                        System.out.println(b.getName());
                        java.util.List<ClassGrip> grips = Arrays.asList(ClassGrip.GRIP_N, ClassGrip.GRIP_NE, ClassGrip.GRIP_E, ClassGrip.GRIP_SE, ClassGrip.GRIP_S, ClassGrip.GRIP_SW, ClassGrip.GRIP_W, ClassGrip.GRIP_NW);
                        java.util.List<java.util.List<Integer>> gripsPositions = b.getGripsPosition();
                        g2.setColor(Color.RED);
                        g2.drawLine(gripsPositions.get(grips.indexOf(_gripSelected)).get(0) + gripSize / 2, gripsPositions.get(grips.indexOf(_gripSelected)).get(1) + gripSize / 2, _drawPanel.getMousePosition().x, _drawPanel.getMousePosition().y);
                    }
                    //Récupération de la liste des positions des points d'accroche pour la classe en cours
                    java.util.List<java.util.List<Integer>> gripsPositions = a.getGripsPosition();
                    for (Link l : _umlDiagramController.getLinksList()) {
                        //Si le lien est sélectionné, on le dessine en rouge, sinon en noir
                        if (_selected instanceof Link && (Link) _selected == l) g2.setColor(Color.RED);
                        else g2.setColor(Color.BLACK);
                        java.util.List<ClassGrip> grips = Arrays.asList(ClassGrip.GRIP_N, ClassGrip.GRIP_NE, ClassGrip.GRIP_E, ClassGrip.GRIP_SE, ClassGrip.GRIP_S, ClassGrip.GRIP_SW, ClassGrip.GRIP_W, ClassGrip.GRIP_NW);
                        java.util.List<java.util.List<Integer>> gripsPositionsStart = l.getStart().getGripsPosition();
                        java.util.List<java.util.List<Integer>> gripsPositionsEnd = l.getEnd().getGripsPosition();
                        int startX = gripsPositionsStart.get(grips.indexOf(l.getGripStart())).get(0);
                        int startY = gripsPositionsStart.get(grips.indexOf(l.getGripStart())).get(1);
                        int endX = gripsPositionsEnd.get(grips.indexOf(l.getGripEnd())).get(0);
                        int endY = gripsPositionsEnd.get(grips.indexOf(l.getGripEnd())).get(1);
                        //Dessin du lien en fonction du type de la relation
                        g2.setStroke(new BasicStroke(1f));
                        //Dessin de la ligne en pointillés si le lien est faible
                        if (l.getType() == Link.LinkType.WEAK) {
                            g2.setStroke(dashed);
                            g2.drawLine(startX, startY, endX, endY);
                        } else if (l.getType() == Link.LinkType.STRONG) g2.drawLine(startX, startY, endX, endY);

                            //Si lien de composition, d'agrégation ou d'héritage, on dessine soit un carré ou un triangle
                        else {
                            //On crée le carré ou le rectangle
                            int[][] posGrip = {{startX, startY}};
                            //Détermination de l'angle pour que la figure s'adapte à la direction du lien
                            float angle = (float) Math.atan2(endX - startX, startY - endY);
                            //Détermination des points des polygones en fonction de l'angle et de la position du point d'accroche
                            float[][] posPol = getPolygonPoints(posGrip, -angle, l.getType());
                            int[] polygonX = new int[posPol.length];
                            int[] polygonY = new int[posPol.length];
                            for (int i = 0; i < posPol.length; i++) {
                                polygonX[i] = (int) posPol[i][0];
                                polygonY[i] = (int) posPol[i][1];
                            }
                            //On dessine un polygone vide en agrégation
                            if (l.getType() == Link.LinkType.AGGREGATION || l.getType() == Link.LinkType.INHERITANCE)
                                g2.drawPolygon(polygonX, polygonY, posPol.length);
                                //On dessine un polygone plein sinon
                            else g2.fillPolygon(polygonX, polygonY, posPol.length);
                            //Adaptation de la ligne sur le 3e point du polygone
                            g2.drawLine(polygonX[2], polygonY[2], endX, endY);
                        }
                        //Affichage des informations du lien
                        if (l.getType() != Link.LinkType.INHERITANCE) {
                            //Décalage des positions pour mettre en évidence les écritures
                            if (l.getGripStart() == ClassGrip.GRIP_NE || l.getGripStart() == ClassGrip.GRIP_E || l.getGripStart() == ClassGrip.GRIP_SE)
                                startX += 20;
                            else if (l.getGripStart() == ClassGrip.GRIP_NW || l.getGripStart() == ClassGrip.GRIP_W || l.getGripStart() == ClassGrip.GRIP_SW)
                                startX -= 20;
                            else if (l.getGripStart() == ClassGrip.GRIP_S) startY += 20;
                            if (l.getGripEnd() == ClassGrip.GRIP_NE || l.getGripEnd() == ClassGrip.GRIP_E || l.getGripEnd() == ClassGrip.GRIP_SE)
                                endX += 20;
                            else if (l.getGripEnd() == ClassGrip.GRIP_NW || l.getGripEnd() == ClassGrip.GRIP_W || l.getGripEnd() == ClassGrip.GRIP_SW)
                                endX -= 20;
                            else if (l.getGripEnd() == ClassGrip.GRIP_S) endY += 20;
                            //Affichage des cardinalités sur la fin du lien
                            String minEndCard = (l.getMinCardinalityEnd() == Link.CARD_UNLIMITED) ? "*" : String.valueOf(l.getMinCardinalityEnd());
                            String maxEndCard = (l.getMaxCardinalityEnd() == Link.CARD_UNLIMITED) ? "*" : String.valueOf(l.getMaxCardinalityEnd());
                            g2.drawString(((minEndCard != maxEndCard) ? minEndCard + ".." + maxEndCard : minEndCard), endX, endY);
                            if (l.getType() == Link.LinkType.STRONG || l.getType() == Link.LinkType.WEAK) {
                                String minStartCard = (l.getMinCardinalityStart() == Link.CARD_UNLIMITED) ? "*" : String.valueOf(l.getMinCardinalityStart());
                                String maxStartCard = (l.getMaxCardinalityStart() == Link.CARD_UNLIMITED) ? "*" : String.valueOf(l.getMaxCardinalityStart());
                                g2.drawString(((minStartCard != maxStartCard) ? minStartCard + ".." + maxStartCard : minStartCard), startX, startY);
                                g2.drawString(l.getName(), (startX + endX) / 2, (startY + endY) / 2);
                            }
                        }

                    }
                    g2.setStroke(new BasicStroke(1f));
                    //Affichage des points d'accroche
                    if (_viewGrips || (_selected instanceof Class && a == (Class) _selected)) {
                        //Changement de la couleur en fonction du mode
                        g2.setColor((_viewGrips) ? Color.GREEN : new Color(39, 76, 94));
                        //Top
                        g2.fillOval(gripsPositions.get(0).get(0) - gripSize / 2, gripsPositions.get(0).get(1) - gripSize / 2, gripSize, gripSize);
                        //Top Right
                        g2.fillOval(gripsPositions.get(1).get(0) - gripSize / 2, gripsPositions.get(1).get(1) - gripSize / 2, gripSize, gripSize);
                        //Right
                        g2.fillOval(gripsPositions.get(2).get(0) - gripSize / 2, gripsPositions.get(2).get(1) - gripSize / 2, gripSize, gripSize);
                        //Bottom Right
                        g2.fillOval(gripsPositions.get(3).get(0) - gripSize / 2, gripsPositions.get(3).get(1) - gripSize / 2, gripSize, gripSize);
                        //Bottom
                        g2.fillOval(gripsPositions.get(4).get(0) - gripSize / 2, gripsPositions.get(4).get(1) - gripSize / 2, gripSize, gripSize);
                        //Bottom Left
                        g2.fillOval(gripsPositions.get(5).get(0) - gripSize / 2, gripsPositions.get(5).get(1) - gripSize / 2, gripSize, gripSize);
                        //Left
                        g2.fillOval(gripsPositions.get(6).get(0) - gripSize / 2, gripsPositions.get(6).get(1) - gripSize / 2, gripSize, gripSize);
                        //Top Left
                        g2.fillOval(gripsPositions.get(7).get(0) - gripSize / 2, gripsPositions.get(7).get(1) - gripSize / 2, gripSize, gripSize);
                    }
                }
            }
        };
        _drawPanel.setBackground(new Color(218, 233, 244));
        _drawPanel.setPreferredSize(new Dimension(_sizeX, _sizeY));
        _drawPanel.addMouseListener(this);
        _drawPanel.addMouseMotionListener(this);
        JScrollPane scroll = new JScrollPane(_drawPanel);
        this.add(scroll, BorderLayout.CENTER);
    }

    /**
     * Redessine les classes et les liens UML.
     */
    public void redraw() {
        _drawPanel.repaint();
    }

    /**
     * Calcule les points du polygone à tracer.
     *
     * @param position La position du point d'accroche.
     * @param angle    L'angle donné par la direction du lien.
     * @param type     Le type de relation.
     * @return Les points du polygone à tracer.
     */
    public float[][] getPolygonPoints(int[][] position, double angle, Link.LinkType type) {
        //Patterns de polygon : carré pour composition et agrégation, triangle pour héritage
        int[][] squarePattern = {{0, 0}, {-10, -10}, {0, -20}, {10, -10}, {0, 0}};
        int[][] trianglePattern = {{0, 0}, {-10, -20}, {0, -20}, {10, -20}, {0, 0}};

        //Demande d'un carré ou d'un triangle selon le type de lien
        float[][] points = null;
        switch (type) {
            case INHERITANCE:
                points = rotateMatrice(trianglePattern, angle);
                break;
            default:
                points = rotateMatrice(squarePattern, angle);
                break;
        }
        //Déplacement du pattern à la position du point d'accroche
        for (int i = 0; i < points.length; i++) {
            points[i][0] += position[0][0];
            points[i][1] += position[0][1];
        }
        return points;
    }

    /**
     * Rotate le polygone sous forme de matrice.
     *
     * @param matrice Les points du pattern de polygone.
     * @param angle   L'angle de rotation.
     * @return La matrice tournée correspondant au polygone final.
     */
    public float[][] rotateMatrice(int[][] matrice, double angle) {
        //Matrice de rotation en fonction de l'angle
        double[][] rotate = {{Math.cos(angle), (-1) * Math.sin(angle)}, {Math.sin(angle), Math.cos(angle)}};
        //Matrice finale
        float[][] finalMatrice = new float[matrice.length][2];
        //Multiplication de rotate * finalMatrice
        for (int i = 0; i < matrice.length; i++) {
            for (int j = 0; j < 2; j++) {
                finalMatrice[i][j] = 0;
                for (int k = 0; k < 2; k++) {
                    finalMatrice[i][j] += matrice[i][k] * rotate[k][j];
                }
            }
        }
        return finalMatrice;
    }

    /**
     * Retourne si la vue des points d'accroches est activée pour le diagramme
     *
     * @return true : la vue des points d'accroche est activée, false sinon.
     */
    public boolean viewGripsEnabled() {
        return _viewGrips;
    }

    /**
     * Active/Désactive la vue des points d'accroches pour les classes.
     *
     * @param enabled La vue des points d'accroches pour les classes.
     */
    public void setViewGripsEnabled(boolean enabled) {
        _viewGrips = enabled;
        redraw();
    }

    /**
     * Retire une classe de la composition UML.
     *
     * @param c La classe à retirer.
     */
    public void removeClass(Class c) {
        //On supprime la classe principale
        _umlDiagramController.removeClass(c);
        _selected = null;
    }

    /**
     * Retire un lien de la composition UML.
     *
     * @param l Le lien à retirer.
     */
    public void removeLink(Link l) {
        //On supprime la classe principale
        _umlDiagramController.removeLink(l);
        _selected = null;
    }

    //Evenements souris

    /**
     * Récupère l'élément sélectionné dans la grille.
     *
     * @return L'élément sélectionné dan sla grille.
     */
    public Object getSelected() {
        return this._selected;
    }

    /**
     * Récupération de l'objet cliqué
     *
     * @param getX Les coordonnées de la souris sur l'axe X.
     * @param getY Les coordonnées de la souris sur l'axe Y.
     */
    public void select(int getX, int getY) {
        //Si un élément est survolé, on le sélectionne
        for (Class c : _umlDiagramController.getClassesList()) {
            int posX = c.getPosX();
            int posY = c.getPosY();
            int sizeX = c.getSizeX();
            int sizeY = c.getSizeY();
            //Si cette classe est survolée, on affiche son nom dans la barre de statut et on la sauvegarde temporairement
            _selected = null;
            if (getX >= posX - sizeX / 2 - gripSize && getX <= posX + sizeX / 2 + gripSize && getY >= posY - sizeY / 2 - gripSize && getY <= posY + sizeY / 2 + gripSize) {
                _atCompositionListener.statusEmitted("Selected class : " + c.getName());
                _selected = c;
                //Si on est en mode édition de liens, on recherche le point d'accroche survolé
                if (_viewGrips) {
                    checkHoveredGrip((Class) _selected, getX, getY);
                    _gripSelected = _gripHovered;
                }
                break;
            }
        }
        for (Link l : _umlDiagramController.getLinksList()) {
            java.util.List<ClassGrip> grips = Arrays.asList(ClassGrip.GRIP_N, ClassGrip.GRIP_NE, ClassGrip.GRIP_E, ClassGrip.GRIP_SE, ClassGrip.GRIP_S, ClassGrip.GRIP_SW, ClassGrip.GRIP_W, ClassGrip.GRIP_NW);
            java.util.List<java.util.List<Integer>> positionsStart = l.getStart().getGripsPosition();
            java.util.List<java.util.List<Integer>> positionsEnd = l.getEnd().getGripsPosition();
            float startX = positionsStart.get(grips.indexOf(l.getGripStart())).get(0);
            float endX = positionsEnd.get(grips.indexOf(l.getGripEnd())).get(0);
            float startY = positionsStart.get(grips.indexOf(l.getGripStart())).get(1);
            float endY = positionsEnd.get(grips.indexOf(l.getGripEnd())).get(1);
            float xc = getX - startX;
            float yc = getY - startY;
            float coeff = (endY - startY) / (endX - startX);
            float y = xc * coeff;
            if (y <= yc + 5 && y >= yc - 5) {
                System.out.println("Le lien " + l.getName() + " a été cliqué !");
                _selected = l;
            }
        }
        //Si la classe est sélectionnée, on enregistre le décalage entre la souris et le centre de la classe
        if (_selected instanceof Class) {
            Class a = (Class) _selected;
            _shiftX = getX - a.getPosX();
            _shiftY = getY - a.getPosY();
        }
        redraw();
    }

    /**
     * Modifie les propriétés de la classe.
     *
     * @param a La classe à modifier.
     */
    public void editClass(Class a) {
        _umlDiagramController.showClassWindow(a);
    }

    /**
     * Modifie les propriétés du lien.
     *
     * @param l Le lien à modifier.
     */
    public void editLink(Link l) {
        _umlDiagramController.showLinkWindow(l);
    }

    /**
     * Vérifie si le curseur est à proximité d'un point d'accroche. Dans ce cas, il enregistre le point survolé.
     *
     * @param a La classe à vérifier.
     * @param x L'abscisse du curseur.
     * @param y L'ordonnée du curseur.
     */
    public void checkHoveredGrip(Class a, int x, int y) {
        int sensibility = 5;
        int posX = a.getPosX();
        int posY = a.getPosY();
        int sizeX = a.getSizeX();
        int sizeY = a.getSizeY();
        //Si la souris est à proximité d'un point d'accroche, on affiche le curseur de redimensionnement
        if (x >= posX - sensibility && x <= posX + sensibility && y >= posY - sizeY / 2 - sensibility && y <= posY - sizeY / 2 + sensibility) {
            _gripHovered = ClassGrip.GRIP_N;
            this.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
        } else if (x >= posX + sizeX / 2 - sensibility && x <= posX + sizeX / 2 + sensibility && y >= posY - sizeY / 2 - sensibility && y <= posY - sizeY / 2 + sensibility) {
            _gripHovered = ClassGrip.GRIP_NE;
            this.setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
        } else if (x >= posX + sizeX / 2 - sensibility && x <= posX + sizeX / 2 + sensibility && y >= posY - sensibility && y <= posY + sensibility) {
            _gripHovered = ClassGrip.GRIP_E;
            this.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
        } else if (x >= posX + sizeX / 2 - sensibility && x <= posX + sizeX / 2 + sensibility && y >= posY + sizeY / 2 - sensibility && y <= posY + sizeY / 2 + sensibility) {
            _gripHovered = ClassGrip.GRIP_SE;
            this.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
        } else if (x >= posX - sensibility && x <= posX + sensibility && y >= posY + sizeY / 2 - sensibility && y <= posY + sizeY / 2 + sensibility) {
            _gripHovered = ClassGrip.GRIP_S;
            this.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
        } else if (x >= posX - sizeX / 2 - sensibility && x <= posX - sizeX / 2 + sensibility && y >= posY + sizeY / 2 - sensibility && y <= posY + sizeY / 2 + sensibility) {
            _gripHovered = ClassGrip.GRIP_SW;
            this.setCursor(new Cursor(Cursor.SW_RESIZE_CURSOR));
        } else if (x >= posX - sizeX / 2 - sensibility && x <= posX - sizeX / 2 + sensibility && y >= posY - sensibility && y <= posY + sensibility) {
            _gripHovered = ClassGrip.GRIP_W;
            this.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
        } else if (x >= posX - sizeX / 2 - sensibility && x <= posX - sizeX / 2 + sensibility && y >= posY - sizeY / 2 - sensibility && y <= posY - sizeY / 2 + sensibility) {
            _gripHovered = ClassGrip.GRIP_NW;
            this.setCursor(new Cursor(Cursor.NW_RESIZE_CURSOR));
        } else {
            _gripHovered = null;
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    //Actions de la souris sur le diagramme UML

    /**
     * Gestion du double clic sur le diagramme uml (pour l'édition).
     *
     * @param e L'événement souris.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        UmlToolbar.UmlTool tool = _umlToolbar.getCurrentTool();
        if (e.getClickCount() == 2 && tool == UmlToolbar.UmlTool.SELECT_TOOL) {
            if (_selected instanceof Class) this.editClass((Class) _selected);
            else if (_selected instanceof Link) this.editLink((Link) _selected);
        }
    }

    /**
     * Gestion du clic simple sur le diagramme uml en fonction de l'outil choisi.
     *
     * @param e L'événement souris.
     */
    @Override
    public void mousePressed(MouseEvent e) {
        UmlToolbar.UmlTool tool = _umlToolbar.getCurrentTool();
        switch (tool) {
            //On essaie de sélectionner un élément
            case EDIT_TOOL:
                System.out.println("On édite un élément.");
                this.select(e.getX(), e.getY());
                if (_selected instanceof Class) this.editClass((Class) _selected);
                else if (_selected instanceof Link) this.editLink((Link) _selected);
                break;
            case DELETE_TOOL:
                System.out.println("On supprime un élément.");
                this.select(e.getX(), e.getY());
                if (_selected instanceof Class) removeClass((Class) _selected);
                else if (_selected instanceof Link) removeLink((Link) _selected);
                break;
            case COPY_TOOL:
                System.out.println("On copie un élément.");
                break;
            case PASTE_TOOL:
                System.out.println("On colle un élément.");
                break;
            case UNDO_TOOL:
                System.out.println("On annule la dernière opération effectuée.");
                break;
            case REDO_TOOL:
                System.out.println("On refait la dernière opération effectuée.");
                break;
            //On essaie d'ajouter une classe
            case CLASS_TOOL:
                _umlDiagramController.addClass(e.getX(), e.getY());
                break;
            case STRONG_TOOL:
                System.out.println("On ajoute une relation forte.");
                this.select(e.getX(), e.getY());
                break;
            case WEAK_TOOL:
                System.out.println("On ajoute une relation faible.");
                this.select(e.getX(), e.getY());
                break;
            case COMPOSITION_TOOL:
                System.out.println("On ajoute une relation de composition.");
                this.select(e.getX(), e.getY());
                break;
            case AGGREGATION_TOOL:
                System.out.println("On ajoute une relation d'agrégation.");
                this.select(e.getX(), e.getY());
                break;
            case INHERITANCE_TOOL:
                System.out.println("On ajoute une relation d'héritage.");
                this.select(e.getX(), e.getY());
                break;
            default:
                this.select(e.getX(), e.getY());
                break;
        }
        redraw();
    }

    /**
     * La souris est relâchée. On annule la création du lien.
     *
     * @param e L'événement souris.
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        //Si on lâche le curseur en cours de création de liens, on annule la saisie.
        if (_viewGrips) {
            if (_gripSelected != null) {
                _gripSelected = null;
                _selected = null;
                _drawPanel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            }
            redraw();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * Déplacer un élément en cliquant et déplaçant la souris.
     *
     * @param e Evénement souris
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        //Mise à jour des coordonnées de la souris dans la statusbar
        int posX = e.getX();
        int posY = e.getY();
        if (_selected instanceof Class) {
            Class a = (Class) _selected;
            int shiftX = 0;
            int shiftY = 0;
            //Sinon, soit on redimensionne une classe ou on la déplace.
            if (!_viewGrips) {
                if (_gripHovered != null) {
                    switch (_gripHovered) {
                        case GRIP_N:
                            _umlDiagramController.resizeUp(a, posY);
                            break;
                        case GRIP_NE:
                            _umlDiagramController.resizeUp(a, posY);
                            _umlDiagramController.resizeRight(a, posX);
                        case GRIP_E:
                            _umlDiagramController.resizeRight(a, posX);
                            break;
                        case GRIP_SE:
                            _umlDiagramController.resizeDown(a, posY);
                            _umlDiagramController.resizeRight(a, posX);
                        case GRIP_S:
                            _umlDiagramController.resizeDown(a, posY);
                            break;
                        case GRIP_SW:
                            _umlDiagramController.resizeDown(a, posY);
                            _umlDiagramController.resizeLeft(a, posX);
                            break;
                        case GRIP_W:
                            _umlDiagramController.resizeLeft(a, posX);
                            break;
                        case GRIP_NW:
                            _umlDiagramController.resizeUp(a, posY);
                            _umlDiagramController.resizeLeft(a, posX);
                            break;
                    }
                }
                //Sinon on déplace
                else {
                    //On repositionne la classe en prenant en compte le décalage mesuré au clic de la souris
                    _umlDiagramController.setPosX(a, posX - _shiftX);
                    _umlDiagramController.setPosY(a, posY - _shiftY);
                    this.setCursor(new Cursor(Cursor.MOVE_CURSOR));
                }
            }
        }
        //On est en train de créer un lien, on recherche le point d'accroche d'arrivée
        if (_selected != null && _gripSelected != null) {
            Class selected = (Class) _selected;
            for (Class hovered : _umlDiagramController.getClassesList()) {
                int posXSelected = hovered.getPosX();
                int posYSelected = hovered.getPosY();
                int sizeXSelected = hovered.getSizeX();
                int sizeYSelected = hovered.getSizeY();
                //Si cette classe est survolée, on affiche son nom dans la barre de statut et on la sauvegarde temporairement
                _hovered = null;
                if (posX >= posXSelected - sizeXSelected / 2 - gripSize && posX <= posXSelected + sizeXSelected / 2 + gripSize && posY >= posYSelected - sizeYSelected / 2 - gripSize && posY <= posYSelected + sizeYSelected / 2 + gripSize) {
                    _atCompositionListener.statusEmitted("Approach a target grip on this class to create the link : " + hovered.getName());
                    if (hovered != _selected) {
                        _hovered = hovered;
                        //On recherche le point survolé
                        checkHoveredGrip((Class) _hovered, posX, posY);
                        //Si toutes les conditions sont réunies, on crée le lien
                        if (_hovered != null && _gripHovered != null && _selected != null && _gripSelected != null) {
                            //Récupération du type de lien
                            UmlToolbar.UmlTool tool = _umlToolbar.getCurrentTool();
                            Link.LinkType type = (tool == UmlToolbar.UmlTool.STRONG_TOOL) ? Link.LinkType.STRONG : (tool == UmlToolbar.UmlTool.WEAK_TOOL) ? Link.LinkType.WEAK : (tool == UmlToolbar.UmlTool.COMPOSITION_TOOL) ? Link.LinkType.COMPOSITION : (tool == UmlToolbar.UmlTool.AGGREGATION_TOOL) ? Link.LinkType.AGGREGATION : Link.LinkType.INHERITANCE;
                            _umlDiagramController.addLink((Class) _selected, (Class) _hovered, _gripSelected, _gripHovered, 0, 0, 0, 0, type);
                            _selected = null;
                            _gripSelected = null;
                            _gripHovered = null;
                            _hovered = null;
                            this.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
                        }
                    }
                    break;
                }
            }
        }
        redraw();
    }

    /**
     * Déclenchée par le mouvement de la souris, cette fonction permet de récupérer la classe actuellement survolée.
     *
     * @param e L'événement souris.
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        int getX = e.getX();
        int getY = e.getY();
        //Si une classe est sélectionnée, on recherche si le curseur est à proximité d'un point d'accroche
        if (_selected instanceof Class) {
            Class a = (Class) _selected;
            checkHoveredGrip(a, getX, getY);
        }
    }

    //Evenements de la UmlToolbar

    /**
     * L'outil UML sélectionné a été modifié
     *
     * @param newTool Le nouvel outil sélectionné.
     */
    @Override
    public void toolChanged(UmlToolbar.UmlTool newTool) {
        //Changement du curseur du diagramme UML
        if (newTool == UmlToolbar.UmlTool.CLASS_TOOL ||
                newTool == UmlToolbar.UmlTool.STRONG_TOOL ||
                newTool == UmlToolbar.UmlTool.WEAK_TOOL ||
                newTool == UmlToolbar.UmlTool.COMPOSITION_TOOL ||
                newTool == UmlToolbar.UmlTool.AGGREGATION_TOOL ||
                newTool == UmlToolbar.UmlTool.INHERITANCE_TOOL) _drawPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        else _drawPanel.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        //Affichage des grips des classes pour l'édition des liens
        if (newTool == UmlToolbar.UmlTool.STRONG_TOOL ||
                newTool == UmlToolbar.UmlTool.WEAK_TOOL ||
                newTool == UmlToolbar.UmlTool.COMPOSITION_TOOL ||
                newTool == UmlToolbar.UmlTool.AGGREGATION_TOOL ||
                newTool == UmlToolbar.UmlTool.INHERITANCE_TOOL) this.setViewGripsEnabled(true);
        else this.setViewGripsEnabled(false);
    }

    /**
     * Affiche la description de l'outil UML
     *
     * @param description La description de l'outil sélectionné
     */
    @Override
    public void showToolDescription(String description) {
        _atCompositionListener.statusEmitted(description);
    }
}
