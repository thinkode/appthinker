package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.controllers.LinkPropertiesController;
import com.thinkode.appthinker.models.Link;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Classe permettant la création de fenêtres pour la modification des propriétés des classes.
 *
 * @author V.BOULANGER
 */
public class LinkPropertiesWindow extends JDialog {

    private LinkPropertiesController _linkPropertiesController;

    private JPanel _generalPanel;
    private JLabel _fromClass;
    private JLabel _toClass;
    private JButton _switchDirection;
    private JRadioButton _strongRelation;
    private JRadioButton _weakRelation;
    private JRadioButton _compositionRelation;
    private JRadioButton _aggregationRelation;
    private JRadioButton _inheritanceRelation;

    private JTextField _txtName;
    private JSpinner _minCardinalityStart;
    private JSpinner _maxCardinalityStart;
    private JCheckBox _maxStartLimited;
    private JSpinner _minCardinalityEnd;
    private JSpinner _maxCardinalityEnd;
    private JCheckBox _maxEndLimited;
    private JPanel _contentPanel;

    /**
     * Constructeur - Crée une instance de la fenêtre de propriétés de lin à partir d'un diagramme et du lien à modifier.
     */
    public LinkPropertiesWindow() {
    }

    /**
     * Charge les informations dans la fenêtre
     */
    public void initializeGraphics() {
        //Paramétrage de la fenêtre
        this.setTitle("Edit properties - " + _linkPropertiesController.getLinkName());
        this.setModal(true);
        this.setSize(new Dimension(800, 375));
        Image img = null;
        try {
            img = ImageIO.read(AppThinker.class.getResource("com/thinkode/appthinker/img/logoAppThinker.png"));
        } catch (Exception ex) {
        }
        this.setIconImage(img);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());

        this.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            //On enregistre à la fermeture de la fenêtre
            @Override
            public void windowClosing(WindowEvent e) {
                save();
                dispose();
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });

        _generalPanel = new JPanel();
        this.setContentPane(_generalPanel);
        fillWindow();
        this.setVisible(true);
    }

    /**
     * Lie la vue à un contrôleur
     *
     * @param linkPropertiesController La classe contrôleur
     */
    public void setController(LinkPropertiesController linkPropertiesController) {
        _linkPropertiesController = linkPropertiesController;
    }

    /**
     * Charge le contenu de la fenêtre en fonction du type de la relation.
     */
    public void fillWindow() {
        //Espace général de la fenêtre
        this.getContentPane().remove(_generalPanel);
        _generalPanel = new JPanel();
        _generalPanel.setLayout(new BoxLayout(_generalPanel, BoxLayout.Y_AXIS));

        //Espace classe de départ et d'arrivée
        JPanel switchPan = new JPanel();
        switchPan.setLayout(new BoxLayout(switchPan, BoxLayout.X_AXIS));
        JPanel classesPan = new JPanel();
        classesPan.setLayout(new BoxLayout(classesPan, BoxLayout.Y_AXIS));
        _fromClass = new JLabel("From : " + _linkPropertiesController.getLinkStartName());
        classesPan.add(_fromClass);
        _toClass = new JLabel("To : " + _linkPropertiesController.getLinkEndName());
        classesPan.add(_toClass);
        switchPan.add(classesPan);
        _generalPanel.add(switchPan);
        _switchDirection = new JButton("⇅ Switch direction");
        _switchDirection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _linkPropertiesController.switchDirection();
            }
        });
        switchPan.add(_switchDirection);

        //Espace type de relation
        JLabel typeLbl = new JLabel("Type of relation :");
        _generalPanel.add(typeLbl);
        JPanel typeRelation = new JPanel();
        typeRelation.setLayout(new FlowLayout());
        ButtonGroup typeGroup = new ButtonGroup();
        _strongRelation = new JRadioButton("Strong");
        Link.LinkType type = _linkPropertiesController.getLinkType();
        if (type == Link.LinkType.STRONG) _strongRelation.setSelected(true);
        _strongRelation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _linkPropertiesController.setLinkType(Link.LinkType.STRONG);
            }
        });
        typeGroup.add(_strongRelation);
        typeRelation.add(_strongRelation);
        _weakRelation = new JRadioButton("Weak");
        if (type == Link.LinkType.WEAK) _weakRelation.setSelected(true);
        _weakRelation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _linkPropertiesController.setLinkType(Link.LinkType.WEAK);
            }
        });
        typeGroup.add(_weakRelation);
        typeRelation.add(_weakRelation);
        _compositionRelation = new JRadioButton("Composition");
        if (type == Link.LinkType.COMPOSITION) _compositionRelation.setSelected(true);
        _compositionRelation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _linkPropertiesController.setLinkType(Link.LinkType.COMPOSITION);
            }
        });
        typeGroup.add(_compositionRelation);
        typeRelation.add(_compositionRelation);
        _aggregationRelation = new JRadioButton("Aggregation");
        if (type == Link.LinkType.AGGREGATION) _aggregationRelation.setSelected(true);
        _aggregationRelation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _linkPropertiesController.setLinkType(Link.LinkType.AGGREGATION);
            }
        });
        typeGroup.add(_aggregationRelation);
        typeRelation.add(_aggregationRelation);
        _inheritanceRelation = new JRadioButton("Inheritance");
        if (type == Link.LinkType.INHERITANCE) _inheritanceRelation.setSelected(true);
        _inheritanceRelation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _linkPropertiesController.setLinkType(Link.LinkType.INHERITANCE);
            }
        });
        typeGroup.add(_inheritanceRelation);
        typeRelation.add(_inheritanceRelation);

        _generalPanel.add(typeRelation);
        _contentPanel = new JPanel();
        _generalPanel.add(_contentPanel);

        //Affichage du nom de la classe de départ et d'arrivée
        _fromClass.setText("From : " + _linkPropertiesController.getLinkStartName());
        _toClass.setText("To : " + _linkPropertiesController.getLinkEndName());
        //Création du contenu selon le type de relation
        _generalPanel.remove(_contentPanel);
        if (type == Link.LinkType.STRONG || type == Link.LinkType.WEAK) {
            _contentPanel = new JPanel();
            _contentPanel.setLayout(new BoxLayout(_contentPanel, BoxLayout.Y_AXIS));
            //Nom de la relation
            JPanel namePan = new JPanel();
            namePan.setLayout(new BoxLayout(namePan, BoxLayout.X_AXIS));
            JLabel lblName = new JLabel("Name : ");
            namePan.add(lblName);
            _txtName = new JTextField(_linkPropertiesController.getLinkName());
            namePan.add(_txtName);
            _contentPanel.add(namePan);
            //Cardinalités de départ et d'arrivée
            JPanel cardsStart = new JPanel();
            cardsStart.setLayout(new BoxLayout(cardsStart, BoxLayout.X_AXIS));
            JLabel lblCardStart = new JLabel("Cardinality (" + _linkPropertiesController.getLinkStartName() + ") : ");
            cardsStart.add(lblCardStart);
            JLabel minStartLbl = new JLabel(" Min ", SwingConstants.RIGHT);
            cardsStart.add(minStartLbl);
            _minCardinalityStart = new JSpinner();
            _minCardinalityStart.setModel(new SpinnerNumberModel(0, 0, 9999, 1));
            _minCardinalityStart.setValue(_linkPropertiesController.getLinkMinCardinalityStart());
            cardsStart.add(_minCardinalityStart);
            JLabel maxStartLbl = new JLabel(" Max ", SwingConstants.RIGHT);
            cardsStart.add(maxStartLbl);
            _maxCardinalityStart = new JSpinner();
            _maxCardinalityStart.setModel(new SpinnerNumberModel(0, 0, 9999, 1));
            cardsStart.add(_maxCardinalityStart);
            _maxStartLimited = new JCheckBox("Unlimited");
            if (_linkPropertiesController.getLinkMaxCardinalityStart() == Link.CARD_UNLIMITED) {
                _maxCardinalityStart.setEnabled(false);
                _maxStartLimited.setSelected(true);
            } else _maxCardinalityStart.setValue(_linkPropertiesController.getLinkMaxCardinalityStart());
            _maxStartLimited.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    _maxCardinalityStart.setEnabled(!_maxStartLimited.isSelected());
                }
            });
            cardsStart.add(_maxStartLimited);
            _contentPanel.add(cardsStart);
            JPanel cardsEnd = new JPanel();
            cardsEnd.setLayout(new BoxLayout(cardsEnd, BoxLayout.X_AXIS));
            JLabel lblCardEnd = new JLabel("Cardinality (" + _linkPropertiesController.getLinkEndName() + ") : ");
            cardsEnd.add(lblCardEnd);
            JLabel minEndLbl = new JLabel(" Min ", SwingConstants.RIGHT);
            cardsEnd.add(minEndLbl);
            _minCardinalityEnd = new JSpinner();
            _minCardinalityEnd.setModel(new SpinnerNumberModel(0, 0, 9999, 1));
            _minCardinalityEnd.setValue(_linkPropertiesController.getLinkMinCardinalityEnd());
            cardsEnd.add(_minCardinalityEnd);
            JLabel maxEndLbl = new JLabel(" Max ", SwingConstants.RIGHT);
            cardsEnd.add(maxEndLbl);
            _maxCardinalityEnd = new JSpinner();
            _maxCardinalityEnd.setModel(new SpinnerNumberModel(0, 0, 9999, 1));
            cardsEnd.add(_maxCardinalityEnd);
            _maxEndLimited = new JCheckBox("Unlimited");
            if (_linkPropertiesController.getLinkMaxCardinalityEnd() == Link.CARD_UNLIMITED) {
                _maxCardinalityEnd.setEnabled(false);
                _maxEndLimited.setSelected(true);
            } else _maxCardinalityEnd.setValue(_linkPropertiesController.getLinkMaxCardinalityEnd());
            _maxEndLimited.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    _maxCardinalityEnd.setEnabled(!_maxEndLimited.isSelected());
                }
            });
            cardsEnd.add(_maxEndLimited);
            _contentPanel.add(cardsEnd);
        } else if (type == Link.LinkType.COMPOSITION || type == Link.LinkType.AGGREGATION) {
            _contentPanel = new JPanel();
            _contentPanel.setLayout(new BoxLayout(_contentPanel, BoxLayout.Y_AXIS));
            //Cardinalités de départ et d'arrivée
            JPanel cardsEnd = new JPanel();
            cardsEnd.setLayout(new BoxLayout(cardsEnd, BoxLayout.X_AXIS));
            JLabel lblCardEnd = new JLabel("Cardinality (" + _linkPropertiesController.getLinkEndName() + ") : ");
            cardsEnd.add(lblCardEnd);
            JLabel minEndLbl = new JLabel("Min", SwingConstants.RIGHT);
            cardsEnd.add(minEndLbl);
            _minCardinalityEnd = new JSpinner();
            _minCardinalityEnd.setModel(new SpinnerNumberModel(0, 0, 9999, 1));
            _minCardinalityEnd.setValue(_linkPropertiesController.getLinkMinCardinalityEnd());
            cardsEnd.add(_minCardinalityEnd);
            JLabel maxEndLbl = new JLabel("Max", SwingConstants.RIGHT);
            cardsEnd.add(maxEndLbl);
            _maxCardinalityEnd = new JSpinner();
            _maxCardinalityEnd.setModel(new SpinnerNumberModel(0, 0, 9999, 1));
            _maxEndLimited = new JCheckBox("Unlimited");
            if (_linkPropertiesController.getLinkMaxCardinalityEnd() == Link.CARD_UNLIMITED) {
                _maxCardinalityEnd.setEnabled(false);
                _maxEndLimited.setSelected(true);
            } else _maxCardinalityEnd.setValue(_linkPropertiesController.getLinkMaxCardinalityEnd());
            cardsEnd.add(_maxCardinalityEnd);

            _maxEndLimited.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    _maxCardinalityEnd.setEnabled(!_maxEndLimited.isSelected());
                }
            });
            cardsEnd.add(_maxEndLimited);
            _contentPanel.add(cardsEnd);
        }
        _generalPanel.add(_contentPanel);

        this.setContentPane(_generalPanel);
        this.repaint();
        this.revalidate();
        this.pack();
    }

    /**
     * Sauvegarde l'ensemble des propriétés du lien.
     */
    public void save() {
        //Validation des changements pour les JSpinner et sauvegarde du nom de la relation et des cardinalités
        Link.LinkType type = _linkPropertiesController.getLinkType();
        if (type == Link.LinkType.INHERITANCE) {
            _linkPropertiesController.setLinkName("inheritance" + _linkPropertiesController.getLinkId());
            _linkPropertiesController.setLinkMinCardinalityStart(Link.CARD_NULL);
            _linkPropertiesController.setLinkMaxCardinalityStart(Link.CARD_NULL);
            _linkPropertiesController.setLinkMinCardinalityEnd(Link.CARD_NULL);
            _linkPropertiesController.setLinkMaxCardinalityEnd(Link.CARD_NULL);
        } else {
            try {
                _minCardinalityEnd.commitEdit();
                _maxCardinalityEnd.commitEdit();
                _linkPropertiesController.setLinkMinCardinalityEnd((Integer) _minCardinalityEnd.getValue());
                _linkPropertiesController.setLinkMaxCardinalityEnd((_maxEndLimited.isSelected()) ? Link.CARD_UNLIMITED : (Integer) _maxCardinalityEnd.getValue());

                if (type == Link.LinkType.STRONG || type == Link.LinkType.WEAK) {
                    _minCardinalityStart.commitEdit();
                    _maxCardinalityStart.commitEdit();
                    _linkPropertiesController.setLinkName(_txtName.getText());
                    _linkPropertiesController.setLinkMinCardinalityStart((Integer) _minCardinalityStart.getValue());
                    _linkPropertiesController.setLinkMaxCardinalityStart((_maxStartLimited.isSelected()) ? Link.CARD_UNLIMITED : (Integer) _maxCardinalityStart.getValue());
                } else if (type == Link.LinkType.COMPOSITION) {
                    _linkPropertiesController.setLinkName("composition" + _linkPropertiesController.getLinkId());
                    _linkPropertiesController.setLinkMinCardinalityStart(Link.CARD_ONE);
                    _linkPropertiesController.setLinkMaxCardinalityStart(Link.CARD_ONE);
                } else {
                    _linkPropertiesController.setLinkName("aggregation" + _linkPropertiesController.getLinkId());
                    _linkPropertiesController.setLinkMinCardinalityStart(Link.CARD_NULL);
                    _linkPropertiesController.setLinkMaxCardinalityStart(Link.CARD_ONE);
                }
                _linkPropertiesController.refresh();
            } catch (java.text.ParseException e) {
                JOptionPane.showMessageDialog(this, "Please verify values for cardinalities.");
            }
        }
    }
}
