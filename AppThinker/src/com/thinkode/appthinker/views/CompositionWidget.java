package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CompositionWidget extends JTabbedPane implements CompositionListener {

    private ArrayList<String> _titlePans;
    private ArrayList<String> _projectPans;
    private CompositionWidgetListener _listener;

    /**
     * Constructeur de CompositionWidget
     */
    public CompositionWidget() {
        _titlePans = new ArrayList<String>();
        _projectPans = new ArrayList<String>();
    }

    /**
     * Ajoute un écouteur d'évenement
     *
     * @param listener La classe d'écoute
     */
    public void addCompositionWidgetListener(CompositionWidgetListener listener) {
        _listener = listener;
    }

    /**
     * Ajoute un nouvel onglet au composant contenant la composition à ajouter
     *
     * @param title            Le nom de la composition
     * @param project          Le projet incluant la composition
     * @param compositionFrame La vue de la composition
     */
    public void addCompositionFrame(String title, String project, CompositionFrame compositionFrame) {
        //Ajout du contenu de l'onglet
        int tabInsert = this.getSelectedIndex() + 1;
        //System.out.println(this.getTabCount());
        compositionFrame.addCompositionListener(this);
        //Si l'onglet n'existe pas encore, on le crée
        int indexDuplicate = checkIfExist(title, project);
        if (indexDuplicate == -1) {
            //Insertion de l'onglet
            _titlePans.add(tabInsert, title);
            _projectPans.add(tabInsert, project);
            this.insertTab(title, new ImageIcon(AppThinker.class.getResource("img/x16/umlComposition.png")), compositionFrame, "<html><b>" + title + "</b></html>", tabInsert);
        } else tabInsert = indexDuplicate;
        this.setSelectedIndex(tabInsert);
        optimizeNames();
    }

    /**
     * Vérifie que l'onglet à ajouter n'est pas déjà ouvert
     *
     * @param title   Le titre de la composition
     * @param project Le nom du projet
     * @return L'index de l'occurence, -1 si introuvable
     */
    private int checkIfExist(String title, String project) {
        for (int index = 0; index < this.getTabCount(); index++) {
            if (title.equals(_titlePans.get(index)) && project.equals(_projectPans.get(index))) return index;
        }
        return -1;
    }

    /**
     * Crée l'objet graphique du titre de l'onglet
     *
     * @param title Le titre de la composition
     * @param index L'index d'insertion de l'onglet
     * @return Le contenu JPanel créé
     */
    private JPanel createTitlePan(String title, int index) {
        JPanel titlePan = new JPanel();
        titlePan.setOpaque(false);
        titlePan.setLayout(new BorderLayout());
        JLabel compositionIcon = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x16/umlComposition.png")));
        titlePan.add(compositionIcon, BorderLayout.WEST);
        JLabel titleString = new JLabel("  " + title + "  ");
        titlePan.add(titleString, BorderLayout.CENTER);
        JButton titleButton = new JButton(new ImageIcon(AppThinker.class.getResource("img/x16/closeTab.png")));
        titleButton.setBorderPainted(false);
        titleButton.setOpaque(false);
        titleButton.setPreferredSize(new Dimension(20, 20));
        titleButton.setToolTipText("<html>Close <b><i>" + title + "</i></b></html>");
        titleButton.setBackground(new Color(105, 105, 114));
        titleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteCompositionFrame(index);
            }
        });
        titlePan.add(titleButton, BorderLayout.EAST);
        return titlePan;
    }

    /**
     * Vérifie qu'il n'y ait pas 2 compositions ouvertes du même nom. Rajoute le nom de projet pour les différencier dans ce cas.
     */
    private void optimizeNames() {
        for (String title : _titlePans) {
            ArrayList<Integer> occurence = new ArrayList<>();
            for (int j = 0; j < this.getTabCount(); j++) {
                if (_titlePans.get(j).equals(title)) occurence.add(j);
            }
            //La composition ouverte n'a pas de doublon
            if (occurence.size() == 1)
                this.setTabComponentAt(occurence.get(0), createTitlePan(title, occurence.get(0)));
                //La composition ouverte a plusieurs doublons
            else {
                for (int index : occurence) {
                    this.setTabComponentAt(index, createTitlePan(title + " - " + _projectPans.get(index), index));
                }
            }
        }
    }

    /**
     * Met à jour le titre de la composition modifiée
     *
     * @param projectName Le nom du projet
     * @param oldName     L'ancien nom
     * @param newName     Le nouveau nom
     */
    public void updateCompositionTitle(String projectName, String oldName, String newName) {
        for (int i = 0; i < this.getTabCount(); i++) {
            if (_titlePans.get(i).equals(oldName) && _projectPans.get(i).equals(projectName))
                _titlePans.set(i, newName);
        }
        optimizeNames();
    }

    /**
     * Supprime la composition du widget
     *
     * @param projectName Le nom du projet
     * @param composition Le nom de la composition
     */
    public void deleteCompositionFrame(String projectName, String composition) {
        for (int i = 0; i < this.getTabCount(); i++) {
            if (_titlePans.get(i).equals(composition) && _projectPans.get(i).equals(projectName)) {
                _titlePans.remove(i);
                _projectPans.remove(i);
                this.remove(i);
            }
        }
        optimizeNames();
    }

    /**
     * Supprime une vue de composition dans les onglets
     *
     * @param index L'index de la vue de composition à supprimer
     */
    public void deleteCompositionFrame(int index) {
        _titlePans.remove(index);
        _projectPans.remove(index);
        this.remove(index);
        optimizeNames();
    }

    //Evenements de la composition

    /**
     * Affiche un message dans la barre de statut
     *
     * @param status Le message à afficher
     */
    @Override
    public void statusEmitted(String status) {
        _listener.statusEmitted(status);
    }

    /**
     * Commande le rafraîchissement du Workspace
     */
    @java.lang.Override
    public void refreshWorkspaceNeeded() {
        _listener.refreshWorkspaceNeeded();
    }
}
