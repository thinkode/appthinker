package com.thinkode.appthinker.views;

public interface CompositionWidgetListener {
    /**
     * Demande le rafraîchissement du Workspace
     */
    void refreshWorkspaceNeeded();

    /**
     * Affiche un message dans la barre de statut
     *
     * @param message Le message à afficher
     */
    void statusEmitted(String message);
}
