package com.thinkode.appthinker.views;

public interface CompositionListener {

    /**
     * Emission d'un nouveau message de status
     *
     * @param status Le nouveau message de statut
     */
    void statusEmitted(String status);

    /**
     * Need composition save
     */
    void refreshWorkspaceNeeded();
}
