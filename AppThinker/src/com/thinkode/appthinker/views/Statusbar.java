package com.thinkode.appthinker.views;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

/**
 * Affiche une barre de statut au pied de la fenêtre
 *
 * @author V.BOULANGER
 */
public class Statusbar extends JPanel {

    private JLabel _statusLabel;

    /**
     * Constructeur de la classe AppThinkerStatusbar
     */
    public Statusbar() {
        //Création de la statusBar
        this.setBorder(new BevelBorder(BevelBorder.LOWERED));
        this.setLayout(new GridLayout(1, 3));
        _statusLabel = new JLabel();
        _statusLabel.setForeground(Color.WHITE);
        this.add(_statusLabel);
        this.setBackground(Color.GRAY);
        this.setBorder(null);
        setStatusMessage("Create or import a project to start");
    }

    /**
     * Met à jour le texte de statut de la barre de statut.
     *
     * @param statusMessage Le message à afficher.
     */
    public void setStatusMessage(String statusMessage) {
        this._statusLabel.setText(" " + statusMessage);
    }
}
