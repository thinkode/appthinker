package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.models.Composition;
import com.thinkode.appthinker.models.Project;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;

public class Workspace extends JPanel implements ActionListener, MouseListener, MouseMotionListener {

    private final JPanel _actionPanel;
    private final JPanel _homePage;
    private final JPanel _newProject;
    private JPanel _contentPanel;
    private JTree _tree;
    private JScrollPane _scrollPane;
    private WorkspaceListener _listener;

    private DefaultMutableTreeNode _root;
    private JTextField _nameProject;
    private JMenuItem _renameProject;
    private JMenuItem _saveProject;
    private JMenuItem _saveAsProject;
    private JMenuItem _newUmlComposition;
    private JMenuItem _deleteProject;
    private JTextField _nameComposition;
    private JMenuItem _renameComposition;
    private JMenuItem _deleteComposition;

    /**
     * Constructeur de Workspace
     */
    public Workspace() {
        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(300, 10000));
        _actionPanel = new JPanel();
        _actionPanel.setLayout(new BorderLayout());
        _actionPanel.setBackground(new Color(238, 238, 238));
        Border panelBorder = BorderFactory.createLineBorder(new Color(105, 105, 114), 1);
        _actionPanel.setBorder(panelBorder);

        _homePage = new JPanel();
        _homePage.setBackground(new Color(238, 238, 238));
        _homePage.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgHome = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x16/homePage.png")));
        _homePage.add(imgHome);
        _homePage.setToolTipText("Go to home page");
        _homePage.addMouseListener(this);
        _actionPanel.add(_homePage, BorderLayout.WEST);

        JLabel workspace = new JLabel("Workspace");
        workspace.setHorizontalAlignment(JLabel.CENTER);
        _actionPanel.add(workspace, BorderLayout.CENTER);

        _newProject = new JPanel();
        _newProject.setBackground(new Color(238, 238, 238));
        _newProject.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgNewProject = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x16/newProject.png")));
        _newProject.add(imgNewProject);
        _newProject.setToolTipText("Create a new project");
        _newProject.addMouseListener(this);
        _actionPanel.add(_newProject, BorderLayout.EAST);

        this.add(_actionPanel, BorderLayout.NORTH);
        _scrollPane = new JScrollPane();
        this.add(_scrollPane, BorderLayout.CENTER);
    }

    /**
     * Crée l'arborescence des projets dans le Workspace
     *
     * @param projects La liste de projets
     */
    public void refreshTree(java.util.List<Project> projects) {
        //Suppression de la scrollbar
        this.remove(_scrollPane);
        //Aucun projet n'est ouvert, on affiche le message par défaut
        if (projects.size() == 0) {
            _contentPanel = new JPanel();
            _contentPanel.setBackground(new Color(238, 238, 238));
            JLabel lbl2 = new JLabel("No project is open. Click on + to add a project.");
            _contentPanel.add(lbl2);
            _scrollPane = new JScrollPane(_contentPanel);
        }
        //Au moins un projet est ouvert, on construit l'arborescence des projets
        else {
            _root = new DefaultMutableTreeNode("Opened projects");
            for (Project proj : projects) {
                String projectName;
                if (_listener.askForProjectSaved(proj)) projectName = proj.getName() + "*";
                else projectName = proj.getName();
                DefaultMutableTreeNode project = new DefaultMutableTreeNode(projectName);
                for (Composition comp : proj.getCompositions()) {
                    String lbl = (comp.isNeededToSave()) ? "<html><i>" + comp.getName() + "</i> (<b>" + comp.getClass().getSimpleName() + "</b>)</html>" : "<html>" + comp.getName() + " (<b>" + comp.getClass().getSimpleName() + "</b>)</html>";
                    DefaultMutableTreeNode composition = new DefaultMutableTreeNode(lbl);
                    project.add(composition);
                }
                _root.add(project);
            }
            _tree = new JTree(_root);
            DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) _tree.getCellRenderer();
            renderer.setTextSelectionColor(Color.WHITE);
            renderer.setBackgroundSelectionColor(new Color(63, 169, 245));
            renderer.setBorderSelectionColor(new Color(63, 169, 245));
            //Ouvrir tous les noeuds
            for (int i = 0; i < _tree.getRowCount(); i++) {
                _tree.expandRow(i);
            }
            JPopupMenu projectContextMenu = new JPopupMenu("Project actions");
            JMenuItem addMenu = new JMenu("New");
            addMenu.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/newComposition.png")));
            _newUmlComposition = new JMenuItem("UML Diagram");
            _newUmlComposition.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/umlComposition.png")));
            _newUmlComposition.addActionListener(this);
            addMenu.add(_newUmlComposition);
            projectContextMenu.add(addMenu);
            projectContextMenu.add(new JSeparator());
            _nameProject = new JTextField("");
            _nameProject.setHorizontalAlignment(JLabel.CENTER);
            projectContextMenu.add(_nameProject);
            _renameProject = new JMenuItem("Rename");
            _renameProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/rename.png")));
            _renameProject.addActionListener(this);
            projectContextMenu.add(_renameProject);
            _saveProject = new JMenuItem("Save");
            _saveProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/saveProject.png")));
            _saveProject.addActionListener(this);
            projectContextMenu.add(_saveProject);
            _saveAsProject = new JMenuItem("Save As...");
            _saveAsProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/saveAsProject.png")));
            _saveAsProject.addActionListener(this);
            projectContextMenu.add(_saveAsProject);
            _deleteProject = new JMenuItem("Close project");
            _deleteProject.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/closeProject.png")));
            _deleteProject.addActionListener(this);
            projectContextMenu.add(_deleteProject);
            _tree.add(projectContextMenu);

            JPopupMenu compositionContextMenu = new JPopupMenu("Composition actions");
            _nameComposition = new JTextField("");
            _nameComposition.setHorizontalAlignment(JLabel.CENTER);
            compositionContextMenu.add(_nameComposition);
            _renameComposition = new JMenuItem("Rename");
            _renameComposition.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/rename.png")));
            _renameComposition.addActionListener(this);
            compositionContextMenu.add(_renameComposition);
            _deleteComposition = new JMenuItem("Delete composition");
            _deleteComposition.setIcon(new ImageIcon(AppThinker.class.getResource("img/x16/deleteComposition.png")));
            _deleteComposition.addActionListener(this);
            compositionContextMenu.add(_deleteComposition);
            _tree.add(compositionContextMenu);

            MouseListener ml = new MouseAdapter() {
                public void mousePressed(MouseEvent e) {
                    int selRow = _tree.getRowForLocation(e.getX(), e.getY());
                    TreePath selPath = _tree.getPathForLocation(e.getX(), e.getY());
                    if (selRow != -1) {
                        int path = selPath.getPathCount();
                        _tree.setSelectionPath(selPath);
                        DefaultMutableTreeNode selectedItem = (DefaultMutableTreeNode) _tree.getSelectionPath().getLastPathComponent();
                        if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON3) {
                            //Clic droit - Affichage des paramètres du projet/composition
                            if (path == 2) {
                                projectContextMenu.show(_tree, e.getX(), e.getY());
                                _nameProject.setText(_listener.askForProjectName(_root.getIndex(selectedItem)));
                            } else {
                                compositionContextMenu.show(_tree, e.getX(), e.getY());
                                _nameComposition.setText(_listener.askForCompositionName(_root.getIndex(selectedItem.getParent()), selectedItem.getParent().getIndex(selectedItem)));
                            }
                        } else if (e.getClickCount() == 2) {
                            if (path == 2) {
                                //Double-clic sur un projet
                            } else {
                                //Double clic sur une composition
                                int compositionId = selectedItem.getParent().getIndex(selectedItem);
                                int projectId = _root.getIndex(selectedItem.getParent());
                                _listener.compositionDoubleClick(projectId, compositionId);
                            }
                        }
                    }
                }
            };
            _tree.addMouseListener(ml);
            _tree.setRootVisible(false);
            _scrollPane = new JScrollPane(_tree);
        }
        this.add(_scrollPane, BorderLayout.CENTER);
        this.revalidate();
    }

    //Evenements du Workspace

    /**
     * Ajoute un écouteur d'événement
     *
     * @param listener La classe écouteur
     */
    public void addWorkspaceListener(WorkspaceListener listener) {
        _listener = listener;
    }

    //Evenements souris

    /**
     * Gestion des clics sur les boutons
     *
     * @param e L'événement souris
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JPanel) {
            JPanel pan = (JPanel) obj;
            if (pan == _homePage) _listener.homePageClicked();
            else if (pan == _newProject) _listener.newBlankProjectClicked();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Grise le bouton lors du survol de la souris
     *
     * @param e L'événement souris
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JPanel) {
            JPanel pan = (JPanel) obj;
            if (pan == _homePage || pan == _newProject) pan.setBackground(new Color(63, 169, 245));
        }
    }

    /**
     * Dégrise le bouton lors du survol de la souris
     *
     * @param e L'événement souris
     */
    @Override
    public void mouseExited(MouseEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JPanel) {
            JPanel pan = (JPanel) obj;
            if (pan == _homePage || pan == _newProject) pan.setBackground(new Color(238, 238, 238));
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    //Clic sur un JMenuItem

    /**
     * Gestion des clics sur le menu contextuel
     *
     * @param e L'événement souris
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JMenuItem) {
            JMenuItem item = (JMenuItem) obj;
            DefaultMutableTreeNode selectedItem = (DefaultMutableTreeNode) _tree.getSelectionPath().getLastPathComponent();
            if (item == _newUmlComposition) _listener.addUmlComposition(_root.getIndex(selectedItem));
            else if (item == _renameProject) {
                if (_nameProject.getText() != null && _nameProject.getText() != "") {
                    _listener.renameProject(_root.getIndex(selectedItem), _nameProject.getText());
                }
            } else if (item == _saveProject) {
                _listener.saveProjectClicked(_root.getIndex(selectedItem));
            } else if (item == _saveAsProject) {
                _listener.saveAsProjectClicked(_root.getIndex(selectedItem));
            } else if (item == _deleteProject) {
                _listener.deleteProject(_root.getIndex(selectedItem));
            } else if (item == _renameComposition) {
                if (_nameComposition.getText() != null && _nameComposition.getText() != "") {
                    _listener.renameComposition(_root.getIndex(selectedItem.getParent()), selectedItem.getParent().getIndex(selectedItem), _nameComposition.getText());
                }
            } else if (item == _deleteComposition) {
                int compositionId = selectedItem.getParent().getIndex(selectedItem);
                int projectId = _root.getIndex(selectedItem.getParent());
                _listener.deleteComposition(projectId, compositionId);
            }
        }
    }
}
