package com.thinkode.appthinker.views;

import com.thinkode.appthinker.models.Project;

public interface WorkspaceListener {

    /**
     * Clic sur le bouton de la page d'accueil
     */
    void homePageClicked();

    /**
     * Clic sur le bouton d'ajout d'un nouveau projet vide
     */
    void newBlankProjectClicked();

    /**
     * Ouverture d'une composition
     *
     * @param projectListId     Le numéro de projet
     * @param compositionListId Le numéro de composition
     */
    void compositionDoubleClick(int projectListId, int compositionListId);

    /**
     * Ajoute une composition UML à un projet
     *
     * @param projectListId Le numéro de projet
     */
    void addUmlComposition(int projectListId);

    /**
     * Demande le nom du projet sélectionné dans le Workspace
     *
     * @param projectListId Le numéro de projet
     * @return Le nom du projet
     */
    String askForProjectName(int projectListId);

    /**
     * Demande si le projet doit être sauvegardé ou non
     *
     * @param project Le projet concerné
     * @return Booléen représentant l'affirmation
     */
    boolean askForProjectSaved(Project project);

    /**
     * Renomme le projet sélectionné
     *
     * @param projectListId Le numéro de projet
     * @param newName       Le nouveau nom
     */
    void renameProject(int projectListId, String newName);

    /**
     * Sauvegarde le projet
     *
     * @param projectListId Le numéro de projet
     */
    void saveProjectClicked(int projectListId);

    /**
     * Sauvegarde le projet sous
     *
     * @param projectListId Le numéro de projet
     */
    void saveAsProjectClicked(int projectListId);

    /**
     * Supprime un projet via le menu contextuel Projet
     *
     * @param projectListId Le numéro de projet
     */
    void deleteProject(int projectListId);

    /**
     * Demande le nom d'une composition sélectionnée dans le Workspace
     *
     * @param projectListId     Le numéro de projet
     * @param compositionListId Le numéro de composition
     * @return Le nom de la composition
     */
    String askForCompositionName(int projectListId, int compositionListId);

    /**
     * Renomme la composition d'un projet
     *
     * @param projectListId     Le numéro de projet
     * @param compositionListId Le numéro de composition
     * @param newName           Le nouveau nom
     */
    void renameComposition(int projectListId, int compositionListId, String newName);

    /**
     * Supprime une composition d'un projet via le menu contextuel Composition
     *
     * @param projectListId     Le numéro de projet
     * @param compositionListId Le numéro de composition
     */
    void deleteComposition(int projectListId, int compositionListId);
}
