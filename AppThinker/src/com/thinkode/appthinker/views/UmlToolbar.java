package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Affiche une barre d'actions en haut de la fenêtre.
 *
 * @author V.BOULANGER
 */
public class UmlToolbar extends JPanel {

    public enum UmlTool {
        SELECT_TOOL,
        EDIT_TOOL,
        DELETE_TOOL,
        COPY_TOOL,
        PASTE_TOOL,
        UNDO_TOOL,
        REDO_TOOL,
        CLASS_TOOL,
        STRONG_TOOL,
        WEAK_TOOL,
        COMPOSITION_TOOL,
        AGGREGATION_TOOL,
        INHERITANCE_TOOL
    }

    private UmlToolbarListener _atUmlToolbarListener;

    private UmlTool _currentTool = UmlTool.SELECT_TOOL;

    private JPanel _editionPanel;
    private JButton _select;
    private JButton _edit;
    private JButton _delete;
    private JButton _copy;
    private JButton _paste;
    private JButton _undo;
    private JButton _redo;

    private JPanel _modelisationPanel;
    private JButton _newClass;
    private JButton _newStrong;
    private JButton _newWeak;
    private JButton _newComposition;
    private JButton _newAggregation;
    private JButton _newInheritance;

    /**
     * Constructeur de la classe AppThinkerToolbar
     */
    public UmlToolbar() {

        //Création de la Toolbar
        this.setLayout(new GridLayout(2, 2, 10, 0));
        this.setBackground(new Color(69, 69, 72));

        _editionPanel = new JPanel();
        _editionPanel.setLayout(new GridLayout(1, 7));
        _select = new JButton();
        _select.setSize(32, 32);
        _select.setBorderPainted(false);
        _select.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/select.png")));
        _select.setToolTipText("Select item.");
        _select.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.SELECT_TOOL);
            }
        });
        _editionPanel.add(_select);
        _edit = new JButton();
        _edit.setSize(32, 32);
        _edit.setBorderPainted(false);
        _edit.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/edit.png")));
        _edit.setToolTipText("Edit item properties.");
        _edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.EDIT_TOOL);
            }
        });
        _editionPanel.add(_edit);
        _delete = new JButton();
        _delete.setSize(32, 32);
        _delete.setBorderPainted(false);
        _delete.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/delete.png")));
        _delete.setToolTipText("Delete item.");
        _delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.DELETE_TOOL);
            }
        });
        _editionPanel.add(_delete);
        _copy = new JButton();
        _copy.setEnabled(false);
        _copy.setSize(32, 32);
        _copy.setBorderPainted(false);
        _copy.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/copy.png")));
        _copy.setToolTipText("Copy item.");
        _copy.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.COPY_TOOL);
            }
        });
        _editionPanel.add(_copy);
        _paste = new JButton();
        _paste.setEnabled(false);
        _paste.setSize(32, 32);
        _paste.setBorderPainted(false);
        _paste.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/paste.png")));
        _paste.setToolTipText("Paste item.");
        _paste.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.PASTE_TOOL);
            }
        });
        _editionPanel.add(_paste);
        _undo = new JButton();
        _undo.setEnabled(false);
        _undo.setSize(32, 32);
        _undo.setBorderPainted(false);
        _undo.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/undo.png")));
        _undo.setToolTipText("Undo last change.");
        _undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.UNDO_TOOL);
            }
        });
        _editionPanel.add(_undo);
        _redo = new JButton();
        _redo.setEnabled(false);
        _redo.setSize(32, 32);
        _redo.setBorderPainted(false);
        _redo.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/redo.png")));
        _redo.setToolTipText("Redo last change.");
        _redo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.REDO_TOOL);
            }
        });
        _editionPanel.add(_redo);

        _modelisationPanel = new JPanel();
        _modelisationPanel.setLayout(new GridLayout(1, 3));
        _newClass = new JButton();
        _newClass.setSize(32, 32);
        _newClass.setBorderPainted(false);
        _newClass.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/newClass.png")));
        _newClass.setToolTipText("Add a new class.");
        _newClass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.CLASS_TOOL);
            }
        });
        _modelisationPanel.add(_newClass);

        _newStrong = new JButton();
        _newStrong.setSize(32, 32);
        _newStrong.setBorderPainted(false);
        _newStrong.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/strongRelation.png")));
        _newStrong.setToolTipText("<html>Add a <b>strong relation</b>.<br>A <b>strong relation</b> is a relation that will always be verified.<br><i>Eg: Teachers teach courses.</i></html>");
        _newStrong.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.STRONG_TOOL);
            }
        });
        _modelisationPanel.add(_newStrong);

        _newWeak = new JButton();
        _newWeak.setSize(32, 32);
        _newWeak.setBorderPainted(false);
        _newWeak.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/weakRelation.png")));
        _newWeak.setToolTipText("<html>Add a <b>weak relation</b>.<br>A <b>weak relation</b> is a relation that only exists at a particular time.<br><i>Eg: A person has a job when he works for a company.</i></html>");
        _newWeak.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.WEAK_TOOL);
            }
        });
        _modelisationPanel.add(_newWeak);

        _newComposition = new JButton();
        _newComposition.setSize(32, 32);
        _newComposition.setBorderPainted(false);
        _newComposition.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/compositionRelation.png")));
        _newComposition.setToolTipText("<html>Add a <b>composition relation</b>.<br>It corresponds to the verb <b>'to have'</b>, <b>with the uniqueness</b> of the component side.<br><i>Eg: A caddie have 4 wheels.</i></html>");
        _newComposition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.COMPOSITION_TOOL);
            }
        });
        _modelisationPanel.add(_newComposition);

        _newAggregation = new JButton();
        _newAggregation.setSize(32, 32);
        _newAggregation.setBorderPainted(false);
        _newAggregation.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/aggregationRelation.png")));
        _newAggregation.setToolTipText("<html>Add an <b>aggregation relation</b>.<br>It corresponds to the verb <b>'to have'</b>, <b>without the uniqueness</b> of the component side.<br><i>Eg: A room has 4 walls. (One wall may be the wall of another room).</i></html>");
        _newAggregation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.AGGREGATION_TOOL);
            }
        });
        _modelisationPanel.add(_newAggregation);

        _newInheritance = new JButton();
        _newInheritance.setSize(32, 32);
        _newInheritance.setBorderPainted(false);
        _newInheritance.setIcon(new ImageIcon(AppThinker.class.getResource("img/x32/inheritanceRelation.png")));
        _newInheritance.setToolTipText("<html>Add an <b>inheritance relation</b>.<br>A class is <b>a specification</b> of another class.<br><i>Eg: An employer is a person.</i></html>");
        _newInheritance.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setCurrentTool(UmlTool.INHERITANCE_TOOL);
            }
        });
        _modelisationPanel.add(_newInheritance);

        this.add(_editionPanel, BorderLayout.CENTER);
        this.add(_modelisationPanel, BorderLayout.CENTER);

        JLabel editLabel = new JLabel("Editing");
        editLabel.setHorizontalAlignment(JLabel.CENTER);
        editLabel.setForeground(Color.WHITE);
        this.add(editLabel);
        JLabel modelisationLabel = new JLabel("Modeling");
        modelisationLabel.setHorizontalAlignment(JLabel.CENTER);
        modelisationLabel.setForeground(Color.WHITE);
        this.add(modelisationLabel);

        this.setEnabled(false);
    }

    /**
     * Ajoute un écouteur d'événement
     *
     * @param atUmlToolbarListener La classe écouteur
     */
    public void addUmlToolbarListener(UmlToolbarListener atUmlToolbarListener) {
        _atUmlToolbarListener = atUmlToolbarListener;
    }

    /**
     * Récupère l'outil actuellement en fonction.
     *
     * @return L'outil actuellement en fonction.
     */
    public UmlTool getCurrentTool() {
        return this._currentTool;
    }


    /**
     * Change d'outil pour l'édition du diagramme.
     *
     * @param currentTool L'outil cible.
     */
    public void setCurrentTool(UmlTool currentTool) {
        if (currentTool != _currentTool) {
            _currentTool = currentTool;
            _atUmlToolbarListener.toolChanged(_currentTool);
            switch (_currentTool) {
                case EDIT_TOOL:
                    _atUmlToolbarListener.showToolDescription("Edit tool - Click an item to edit its properties.");
                    break;
                case DELETE_TOOL:
                    _atUmlToolbarListener.showToolDescription("Delete tool - Click an item to delete it.");
                    break;
                case COPY_TOOL:
                    _atUmlToolbarListener.showToolDescription("Copy tool - Click an item to copy it.");
                    break;
                case PASTE_TOOL:
                    _atUmlToolbarListener.showToolDescription("Paste tool - Click everywhere to paste the last copied item.");
                    break;
                case UNDO_TOOL:
                    _atUmlToolbarListener.showToolDescription("Undo tool - Undo the last change.");
                    break;
                case REDO_TOOL:
                    _atUmlToolbarListener.showToolDescription("Redo tool - Redo the last change.");
                    break;
                case CLASS_TOOL:
                    _atUmlToolbarListener.showToolDescription("Class tool - Click everywhere to add a new class.");
                    break;
                case STRONG_TOOL:
                    _atUmlToolbarListener.showToolDescription("Strong relation tool - Click on a class, hold, and release on another class.");
                    break;
                case WEAK_TOOL:
                    _atUmlToolbarListener.showToolDescription("Weak relation tool - Click on a class, hold, and release on another class.");
                    break;
                case COMPOSITION_TOOL:
                    _atUmlToolbarListener.showToolDescription("Composition relation tool - Click on a class, hold, and release on another class.");
                    break;
                case AGGREGATION_TOOL:
                    _atUmlToolbarListener.showToolDescription("Aggregation relation tool - Click on a class, hold, and release on another class.");
                    break;
                case INHERITANCE_TOOL:
                    _atUmlToolbarListener.showToolDescription("Inheritance relation tool - Click on a class, hold, and release on another class.");
                    break;
                default:
                    _atUmlToolbarListener.showToolDescription("Select tool - Click an item to select it or move it.");
                    break;
            }
        }
    }
}

