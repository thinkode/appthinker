package com.thinkode.appthinker.views;

import com.thinkode.appthinker.AppThinker;
import com.thinkode.appthinker.controllers.HomeFrameController;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.MouseEvent;

public class HomeFrame extends CompositionFrame {

    private HomeFrameController _homeFrameController;

    private JPanel _newBlankProject;
    private JPanel _newUmlProject;
    private JPanel _openProject;
    private JPanel _proposeIdeas;
    private JPanel _viewDocumentation;
    private JPanel _makeDonation;
    private JCheckBox _dontShow;

    /**
     * Constructeur de HomeFrameController
     */
    public HomeFrame() {
        this.setBackground(new Color(63, 169, 245));
        this.setLayout(new BorderLayout());

        //JPanel du haut
        JPanel _topPanel = new JPanel();
        _topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel logo = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x128/logoAppThinker.png")));
        JPanel _titlePan = new JPanel();
        _titlePan.setLayout(new BoxLayout(_titlePan, BoxLayout.Y_AXIS));
        JLabel title = new JLabel("AppThinker");
        title.setFont(new Font("Montserrat Light", Font.PLAIN, 70));
        _titlePan.add(title);
        JLabel subtitle = new JLabel("Make your ideas come true");
        subtitle.setFont(new Font("Montserrat Light", Font.PLAIN, 20));
        _titlePan.add(subtitle);
        _topPanel.add(logo);
        _topPanel.add(_titlePan);
        this.add(_topPanel, BorderLayout.NORTH);

        //Copyright de bas de composition
        JLabel copyright = new JLabel(AppThinker.copyright + " - Version " + AppThinker.version);
        copyright.setHorizontalAlignment(SwingConstants.CENTER);
        this.add(copyright, BorderLayout.SOUTH);

        //Contenu de la composition
        JPanel content = new JPanel();
        content.setBackground(Color.RED);
        content.setLayout(new BorderLayout());

        JPanel actionPanel = new JPanel();
        actionPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();
        gc.fill = GridBagConstraints.NONE;
        gc.ipady = gc.anchor = GridBagConstraints.CENTER;
        gc.weightx = 3;
        gc.weighty = 4;

        gc.gridx = 1;
        gc.gridy = 0;
        JLabel welcome = new JLabel("Welcome. What do you want to do ?");
        welcome.setFont(new Font("Montserrat Light", Font.PLAIN, 30));
        actionPanel.add(welcome, gc);

        gc.gridx = 1;
        gc.gridy = 1;
        JPanel homeMenu = new JPanel();
        homeMenu.setLayout(new FlowLayout());

        JPanel projectPanel = new JPanel();
        projectPanel.setLayout(new BoxLayout(projectPanel, BoxLayout.Y_AXIS));
        //--Bouton nouveau projet
        _newBlankProject = new JPanel();
        _newBlankProject.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgNewProject = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x32/blankProject.png")));
        _newBlankProject.add(imgNewProject);
        JLabel newProjectLabel = new JLabel("Create a blank project");
        _newBlankProject.setToolTipText("Create a new project without composition");
        _newBlankProject.add(newProjectLabel);
        _newBlankProject.addMouseListener(this);
        projectPanel.add(_newBlankProject);
        //--Bouton nouveau projet UML
        _newUmlProject = new JPanel();
        _newUmlProject.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgNewUmlProject = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x32/umlProject.png")));
        _newUmlProject.add(imgNewUmlProject);
        JLabel newUmlProjectLabel = new JLabel("Create a UML project");
        _newUmlProject.setToolTipText("Create a new project and initialize it with an UML diagram");
        _newUmlProject.add(newUmlProjectLabel);
        _newUmlProject.addMouseListener(this);
        projectPanel.add(_newUmlProject);
        //--Bouton ouverture d'un projet
        _openProject = new JPanel();
        _openProject.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgOpenProject = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x32/importProject.png")));
        _openProject.add(imgOpenProject);
        JLabel openProjectLabel = new JLabel("Open an existing project");
        _openProject.setToolTipText("Choose an AppThinker project from a location");
        _openProject.add(openProjectLabel);
        _openProject.addMouseListener(this);
        projectPanel.add(_openProject);
        homeMenu.add(projectPanel);

        //Panneau de séparation
        JPanel separatePan = new JPanel();
        separatePan.setBackground(Color.GRAY);
        separatePan.setPreferredSize(new Dimension(1, 150));
        homeMenu.add(separatePan);

        //Panneau logiciel
        JPanel softwarePanel = new JPanel();
        softwarePanel.setLayout(new BoxLayout(softwarePanel, BoxLayout.Y_AXIS));
        //--Bouton pour proposer des idées
        _proposeIdeas = new JPanel();
        _proposeIdeas.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgProposeIdeas = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x32/giveFeedback.png")));
        _proposeIdeas.add(imgProposeIdeas);
        JLabel proposeIdeasLabel = new JLabel("Propose your ideas !");
        _proposeIdeas.setToolTipText("You have the voice ! Click here if you want to report a bug or propose ideas for this software");
        _proposeIdeas.addMouseListener(this);
        _proposeIdeas.add(proposeIdeasLabel);
        softwarePanel.add(_proposeIdeas);

        //Bouton pour voir la documentation/le dépôt
        _viewDocumentation = new JPanel();
        _viewDocumentation.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgViewDocumentation = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x32/repository.png")));
        _viewDocumentation.add(imgViewDocumentation);
        JLabel viewDocumentationLabel = new JLabel("View our repository");
        _viewDocumentation.setToolTipText("Visit the repository to read more information, download the latest version and follow the progress of the software");
        _viewDocumentation.addMouseListener(this);
        _viewDocumentation.add(viewDocumentationLabel);
        softwarePanel.add(_viewDocumentation);

        //Bouton pour faire une donation
        _makeDonation = new JPanel();
        _makeDonation.setLayout(new FlowLayout(FlowLayout.LEFT));
        JLabel imgMakeDonation = new JLabel(new ImageIcon(AppThinker.class.getResource("img/x32/donation.png")));
        _makeDonation.add(imgMakeDonation);
        JLabel makeDonationLabel = new JLabel("Make a donation. Thanks !");
        _makeDonation.setToolTipText("Do you enjoy AppThinker ? Please buy us a coffee !");
        _makeDonation.addMouseListener(this);
        _makeDonation.add(makeDonationLabel);
        softwarePanel.add(_makeDonation);
        homeMenu.add(softwarePanel);
        actionPanel.add(homeMenu, gc);

        content.add(actionPanel, BorderLayout.CENTER);

        JScrollPane scroll = new JScrollPane(actionPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        content.add(actionPanel, BorderLayout.CENTER);

        //Panneau ne plus afficher
        _dontShow = new JCheckBox("Do not show this home page at startup");

        _dontShow.addMouseListener(this);
        content.add(_dontShow, BorderLayout.SOUTH);

        this.add(content, BorderLayout.CENTER);
    }

    /**
     * Lie la vue au contrôleur
     *
     * @param homeFrameController La classe contrôleur
     */
    public void setController(HomeFrameController homeFrameController) {
        _homeFrameController = homeFrameController;
        _dontShow.setSelected(!_homeFrameController.isHomeAtStartup());
    }

    //Evenements souris

    /**
     * Gestion des clics sur les boutons
     *
     * @param e L'Evenement souris
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JPanel) {
            JPanel pan = (JPanel) obj;
            if (pan == _newBlankProject) _homeFrameController.newBlankProject();
            else if (pan == _newUmlProject) _homeFrameController.newUmlProject();
            else if (pan == _openProject) {
                FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("AppThinker project", "appt");
                JFileChooser dialog = new JFileChooser();
                dialog.setDialogType(JFileChooser.OPEN_DIALOG);
                dialog.setDialogTitle("Open an AppThinker project");
                dialog.setFileFilter(fileFilter);
                dialog.setAcceptAllFileFilterUsed(false);
                if (dialog.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    _homeFrameController.openProject(dialog.getSelectedFile().getPath());
                }
            } else if (pan == _proposeIdeas) _homeFrameController.giveFeedback();
            else if (pan == _viewDocumentation) _homeFrameController.visitRepository();
            else if (pan == _makeDonation) _homeFrameController.makeDonation();
        } else if (obj instanceof JCheckBox) {
            JCheckBox check = (JCheckBox) obj;

            if (check == _dontShow) _homeFrameController.showHomeAtStartup(_dontShow.isSelected());
        }
    }

    /**
     * Grise les boutons lors du survol de la souris
     *
     * @param e L'evenement souris
     */
    @Override
    public void mouseEntered(MouseEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JPanel) {
            JPanel pan = (JPanel) obj;
            pan.setBackground(new Color(230, 230, 230));
        }
    }

    /**
     * Dégrise les boutons lors du survol de la souris
     *
     * @param e L'événement souris
     */
    @Override
    public void mouseExited(MouseEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JPanel) {
            JPanel pan = (JPanel) obj;
            pan.setBackground(new Color(238, 238, 238));
        }
    }
}
