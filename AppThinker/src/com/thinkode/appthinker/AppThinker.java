package com.thinkode.appthinker;

import com.thinkode.appthinker.controllers.ChangelogWindowController;
import com.thinkode.appthinker.controllers.WindowController;
import com.thinkode.appthinker.views.Splashscreen;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe principale du logiciel AppThinker.
 *
 * @author V.BOULANGER
 */
public class AppThinker {
    //JavaDoc tags : @param @return @throws @author @version @see @since @serial @deprecated

    public static String version = "1.0.0";
    public static String dateVersion = "23/04/2021";
    public static List<String> changelog;
    public static String developer = "V. BOULANGER";
    public static String copyright = "© 2021 - Valentin Boulanger";
    private static ATProperties _appProperties;

    private static Splashscreen _splash;

    /**
     * La méthode principale exécutée.
     *
     * @param args Les arguments de la méthode principale.
     * @throws InterruptedException Interruption du thread lors de la pause du splashscreen.
     */
    public static void main(String[] args) throws InterruptedException {
        //Création de la liste des modifications de la version actuelle
        _appProperties = new ATProperties("app.properties");

        changelog = new ArrayList<String>();
        changelog.add("#16 Workspace implementation");
        changelog.add("#15 Multi-project and multi-composition editing");
        changelog.add("#39 MVC implementation");
        changelog.add("Resolve some graphic bugs");

        _splash = new Splashscreen();

        //Récupération des paramètres du logiciel
        _appProperties.loadPreferencesFromFile();

        Thread.sleep(3000);
        _splash.dispose();

        new WindowController();

        //Si le logiciel s'ouvre pour la 1ère fois, on affiche la fenêtre de changelog
        if (_appProperties.getProperty("isFirstLaunch").equals("true")) {
            _appProperties.setProperty("isFirstLaunch", "false");
            _appProperties.storeConfiguration();
            new ChangelogWindowController();
        }
    }

    /**
     * Renvoie les préférences utilisateur du logiciel
     *
     * @return Les préférences utilisateur
     */
    public static ATProperties getATProperties() {
        return _appProperties;
    }
}
